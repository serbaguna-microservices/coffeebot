package csui.serbagunabot.coffeebot.controller;

import csui.serbagunabot.coffeebot.model.line.LineRequest;
import csui.serbagunabot.coffeebot.model.line.LineResponse;
import csui.serbagunabot.coffeebot.service.CoffeeBotHandlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CoffeeBotController {

    @Autowired
    CoffeeBotHandlerService coffeeBotHandlerService;

    /**
     * Called by the proxy server if coffeebot message was received.
     * @param lineRequest Instance containing message and userId
     */
    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<LineResponse> replyEntrypoint(@RequestBody LineRequest lineRequest) {
        String response = coffeeBotHandlerService
                .handleMessage(lineRequest.getMessage(), lineRequest.getUserId());

        return ResponseEntity.ok(new LineResponse(response));
    }

}
