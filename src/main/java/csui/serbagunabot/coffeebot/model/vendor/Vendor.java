package csui.serbagunabot.coffeebot.model.vendor;

import csui.serbagunabot.coffeebot.model.coffee.Coffee;
import csui.serbagunabot.coffeebot.repository.VendorRepository;
import csui.serbagunabot.coffeebot.repository.VendorRepositoryImpl;
import java.util.List;

public class Vendor {

    private VendorRepository vendorRepository = VendorRepositoryImpl.getUniqueVendorRepository();

    private String name;
    private String location;
    private String description;
    private String id;

    /**
     * Vendor constructor.
     */
    public Vendor(String name, String location, String id) {
        this.name = name;
        this.location = location;
        this.id = id;
        this.description = this.name + ", " + this.location + " (ID: " + this.id + ")";
    }

    /**
     * Get menu of vendor.
     */
    public String getMenu() {
        List<Coffee> listOfCoffee = vendorRepository.getListOfCoffeeOfVendor(this);

        StringBuilder res = new StringBuilder();
        res.append("List Menu ").append(this.description).append(":\n\n");
        for (Coffee coffee : listOfCoffee) {
            res.append("- ")
                    .append(coffee.getDescription())
                    .append(" (Rp ")
                    .append(coffee.cost())
                    .append(")\n");
        }
        return res.toString();
    }

    public String getDescription() {
        return this.description;
    }

    public String getId() {
        return this.id;
    }
}
