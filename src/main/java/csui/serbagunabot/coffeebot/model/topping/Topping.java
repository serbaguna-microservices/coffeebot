package csui.serbagunabot.coffeebot.model.topping;


public class Topping {

    private String name;
    private double cost;

    public Topping(String name, double cost) {
        this.name = name;
        this.cost = cost;
    }

    public String getDescription() {
        return this.name;
    }

    public double cost() {
        return this.cost;
    }

}
