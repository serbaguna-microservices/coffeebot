package csui.serbagunabot.coffeebot.model.topping;

import csui.serbagunabot.coffeebot.model.coffee.Coffee;
import java.util.List;

public class ToppingDecorator implements Coffee {

    private Coffee coffee;
    private Topping topping;
    private List<Topping> allowedToppings;

    /**
     * ToppingDecorator constructor.
     */
    public ToppingDecorator(Coffee coffee, Topping topping) {
        this.coffee = coffee;
        this.topping = topping;
        this.allowedToppings = this.coffee.getAllowedToppings();
    }

    @Override
    public String getDescription() {
        String desc = this.coffee.getDescription();
        int idxLastChar = desc.length() - 1;
        String lastChar = desc.substring(idxLastChar);
        if (lastChar.equals(")")) {
            return desc.substring(0, idxLastChar)
                    + ", "
                    + this.topping.getDescription()
                    + ")";
        }
        return desc + " (" + this.topping.getDescription() + ")";
    }

    @Override
    public double cost() {
        return this.coffee.cost() + this.topping.cost();
    }

    @Override
    public void addToppingToAllowedToppings(Topping topping) {
        this.allowedToppings.add(topping);
    }

    @Override
    public String getAllowedToppingsAsString() {
        return this.coffee.getAllowedToppingsAsString();
    }

    @Override
    public String getIdVendor() {
        return this.coffee.getIdVendor();
    }

    @Override
    public void setIdVendor(String idVendor) {
        this.coffee.setIdVendor(idVendor);
    }

    public List<Topping> getAllowedToppings() {
        return this.allowedToppings;
    }

    public Coffee getCoffee() {
        return this.coffee;
    }

    public void setCoffee(Coffee coffee) {
        this.coffee = coffee;
    }

    public String getToppingDescription() {
        return this.topping.getDescription();
    }
}
