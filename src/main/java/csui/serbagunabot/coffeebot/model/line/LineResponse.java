package csui.serbagunabot.coffeebot.model.line;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class LineResponse {
    private String response;
}

