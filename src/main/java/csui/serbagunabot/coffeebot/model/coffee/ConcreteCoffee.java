package csui.serbagunabot.coffeebot.model.coffee;

import csui.serbagunabot.coffeebot.model.topping.Topping;
import java.util.ArrayList;
import java.util.List;

public class ConcreteCoffee implements Coffee {

    private String name;
    private double cost;
    private List<Topping> allowedToppings;
    private String idVendor;

    /**
     * ConcreteCoffee constructor.
     */
    public ConcreteCoffee(String name, double cost) {
        this.name = name;
        this.cost = cost;
        this.allowedToppings = new ArrayList<>();
    }

    @Override
    public String getDescription() {
        return this.name;
    }

    @Override
    public double cost() {
        return this.cost;
    }

    @Override
    public void addToppingToAllowedToppings(Topping topping) {
        this.allowedToppings.add(topping);
    }

    @Override
    public List<Topping> getAllowedToppings() {
        return this.allowedToppings;
    }

    @Override
    public String getAllowedToppingsAsString() {
        StringBuilder res = new StringBuilder();

        res.append("Topping yang tersedia:");
        for (Topping topping : this.allowedToppings) {
            res.append("\n- ")
                    .append(topping.getDescription())
                    .append(" (Rp ")
                    .append(topping.cost())
                    .append(")");
        }
        return res.toString();
    }

    @Override
    public String getIdVendor() {
        return this.idVendor;
    }

    @Override
    public void setIdVendor(String idVendor) {
        this.idVendor = idVendor;
    }
}