package csui.serbagunabot.coffeebot.model.coffee;

import csui.serbagunabot.coffeebot.model.topping.Topping;
import java.util.List;

public interface Coffee {

    String getDescription();

    double cost();
    
    void addToppingToAllowedToppings(Topping topping);
    
    List<Topping> getAllowedToppings();

    String getAllowedToppingsAsString();

    String getIdVendor();

    void setIdVendor(String idVendor);
}
