package csui.serbagunabot.coffeebot.model.user;


import csui.serbagunabot.coffeebot.chat.states.*;
import csui.serbagunabot.coffeebot.handler.MessagePushHandler;
import csui.serbagunabot.coffeebot.model.coffee.Coffee;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;


@Getter
public class CoffeeBotUser {

    @Autowired
    MessagePushHandler messagePushHandler;

    private CoffeeBotState currentState;
    private HomeCoffeeBotState homeCoffeeBotState;
    private AddToppingCoffeeBotState addToppingCoffeeBotState;
    private OrderConfirmationCoffeeBotState orderConfirmationCoffeeBotState;
    private MakingOrderCoffeeBotState makingOrderCoffeeBotState;
    private WaitingPickUpCoffeeBotState waitingPickUpCoffeeBotState;

    private CoffeeBotUserKeranjang keranjang;
    private String userId;

    /**
     * CoffeeBotUser constructor. Construct a CoffeeBotUser object with all of it's possible states.
     */
    public CoffeeBotUser(String userId) {
        this.homeCoffeeBotState = new HomeCoffeeBotState(this);
        this.addToppingCoffeeBotState = new AddToppingCoffeeBotState(this);
        this.orderConfirmationCoffeeBotState = new OrderConfirmationCoffeeBotState(this);
        this.makingOrderCoffeeBotState = new MakingOrderCoffeeBotState(this);
        this.waitingPickUpCoffeeBotState = new WaitingPickUpCoffeeBotState(this);
        this.currentState = this.homeCoffeeBotState;

        this.keranjang = new CoffeeBotUserKeranjang();
        this.userId = userId;
    }

    /**
     * Handle the command and message from user on Line according to the user's state.
     * Calls doCommand method of the user's current state.
     *
     * @param command command from user on Line.
     * @param message parameter of the command if there is any.
     * @return        response from doCommand of user's current state.
     */
    public String doCommand(String command, String message) {
        return this.currentState.doCommand(command, message);
    }

    public void setCurrentState(CoffeeBotState coffeeBotState) {
        this.currentState = coffeeBotState;
    }

    public String getCurrentStateMessage() {
        return this.currentState.getMessage();
    }

    public void addItemToKeranjang(Coffee coffee) {
        this.keranjang.addItemToKeranjang(coffee);
    }

    public void removeItemFromKeranjang(Coffee coffee) {
        this.keranjang.removeItemFromKeranjang(coffee);
    }

    public void removeItemFromKeranjangByIdx(int idx) {
        this.keranjang.removeItemFromKeranjangByIdx(idx);
    }

    public double getTotalPriceInKeranjang() {
        return this.keranjang.getTotalPriceInKeranjang();
    }

    public String getAllItemInKeranjang() {
        return this.keranjang.getAllItemInKeranjang();
    }

    public int getSizeOfKeranjang() {
        return this.keranjang.getSize();
    }

    public Coffee getItemInKeranjangByIdx(int idx) {
        return this.keranjang.getItemInKeranjangByIdx(idx);
    }

    public void setItemInKeranjangByIdx(int idx, Coffee coffee) {
        this.keranjang.setItemInKeranjangByIdx(idx, coffee);
    }

    public Coffee getLastAddedItemToKeranjang() {
        return this.keranjang.getLastAddedItemToKeranjang();
    }

    public Coffee popLastAddedItemToKeranjang() {
        return this.keranjang.popLastAddedItemToKeranjang();
    }

    public void clearKeranjang() {
        this.keranjang.clearKeranjang();
    }

    public void setMessagePushHandler(MessagePushHandler messagePushHandler) {
        this.messagePushHandler = messagePushHandler;
    }

    public void pushMessage(String message) {
        messagePushHandler.handlePushMessage(message, this.userId);
    }
}
