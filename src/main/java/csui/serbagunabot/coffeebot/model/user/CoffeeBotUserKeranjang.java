package csui.serbagunabot.coffeebot.model.user;

import csui.serbagunabot.coffeebot.model.coffee.Coffee;
import java.util.*;

public class CoffeeBotUserKeranjang {

    private List<Coffee> keranjang;

    public CoffeeBotUserKeranjang() {
        this.keranjang = new ArrayList<>();
    }

    public void addItemToKeranjang(Coffee coffee) {
        this.keranjang.add(coffee);
    }

    public void removeItemFromKeranjang(Coffee coffee) {
        this.keranjang.remove(coffee);
    }

    public void removeItemFromKeranjangByIdx(int idx) {
        this.keranjang.remove(idx);
    }

    /**
     * Get all items in shoppingCart list as String.
     *
     * @return  String that consists of all items in shoppingCart list.
     */
    public String getAllItemInKeranjang() {
        StringBuilder res = new StringBuilder();

        res.append("Keranjang Anda (").append(this.getSize()).append(" item):\n\n");

        if (this.getSize() == 0) {
            res.append("Belum ada minuman dalam keranjang Anda.\n")
                    .append("Silakan memesan dengan menggunakan perintah ")
                    .append("'!coffee pesan id-vendor nama-menu'\n");
            return res.toString();
        }
        int noUrut = 1;
        for (Coffee coffee : this.keranjang) {
            res.append(noUrut)
                    .append(". ")
                    .append(coffee.getDescription())
                    .append("\n<Rp ")
                    .append(coffee.cost())
                    .append(">\n\n");
            noUrut++;
        }
        return res.toString();
    }

    /**
     * Get Total Price from all the items in Keranjang.
     *
     * @return integer of total price.
     */
    public double getTotalPriceInKeranjang() {
        double totalPrice = 0;
        for (Coffee coffee : this.keranjang) {
            totalPrice += coffee.cost();
        }
        return totalPrice;
    }

    public int getSize() {
        return this.keranjang.size();
    }

    public Coffee getItemInKeranjangByIdx(int idx) {
        return this.keranjang.get(idx);
    }

    public void setItemInKeranjangByIdx(int idx, Coffee coffee) {
        this.keranjang.set(idx, coffee);
    }

    public Coffee getLastAddedItemToKeranjang() {
        return this.keranjang.get(this.getSize() - 1);
    }

    public Coffee popLastAddedItemToKeranjang() {
        return this.keranjang.remove(this.getSize() - 1);
    }

    public void clearKeranjang() {
        this.keranjang.clear();
    }
}
