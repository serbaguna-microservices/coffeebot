package csui.serbagunabot.coffeebot.service;

import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import csui.serbagunabot.coffeebot.repository.CoffeeBotUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoffeeBotHandlerService {

    @Autowired
    CoffeeBotUserRepository coffeeBotUserRepository;

    Logger logger = LoggerFactory.getLogger("Reply Logging");

    /**
     * Handles message for coffeebot logic.
     * @param message message from the proxy server
     * @param userId userId as the user's or sender's id
     * @return String as a reply
     */
    public String handleMessage(String message, String userId) {

        String cleanedMessage = message.replace("!coffee", "").trim();
        String command = ((cleanedMessage.contains(" "))
                ? cleanedMessage.substring(0, cleanedMessage.indexOf(" ")).trim()
                : cleanedMessage);
        String parameter = cleanedMessage.replace(command, "").trim();

        CoffeeBotUser coffeeBotUser = coffeeBotUserRepository.findByUserId(userId);
        if (coffeeBotUser == null) {
            coffeeBotUser = coffeeBotUserRepository.createUserFromId(userId);
        }

        return coffeeBotUser.doCommand(command, parameter);
    }

}
