package csui.serbagunabot.coffeebot.chat.observer.subject.async;


import java.util.function.Supplier;
import lombok.SneakyThrows;


public class SubjectAsyncSupplier implements Supplier<String> {

    private int randomWaitTime;

    /**
     * SubjectAsyncSupplier constructor.
     */
    public SubjectAsyncSupplier() {
        this.randomWaitTime = 10000;
    }

    @SneakyThrows
    @Override
    public String get() {
        Thread.sleep(this.randomWaitTime);
        return "Done";
    }

}
