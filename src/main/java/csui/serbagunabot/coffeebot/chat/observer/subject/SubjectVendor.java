package csui.serbagunabot.coffeebot.chat.observer.subject;

import csui.serbagunabot.coffeebot.chat.observer.Observer;
import csui.serbagunabot.coffeebot.chat.observer.subject.async.SubjectAsyncSupplier;
import java.util.concurrent.CompletableFuture;

public class SubjectVendor implements Subject {

    Observer observer;

    public SubjectVendor(Observer observer) {
        this.observer = observer;
    }

    @Override
    public void startProcess() {
        CompletableFuture<String> waitFuture = CompletableFuture
                .supplyAsync(new SubjectAsyncSupplier());

        waitFuture.thenAcceptAsync(result -> notifyObserver());
    }

    @Override
    public void notifyObserver() {
        this.observer.update();
    }
}
