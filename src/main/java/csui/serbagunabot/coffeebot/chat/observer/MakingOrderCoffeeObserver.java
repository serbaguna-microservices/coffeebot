package csui.serbagunabot.coffeebot.chat.observer;

import csui.serbagunabot.coffeebot.chat.observer.subject.Subject;
import csui.serbagunabot.coffeebot.chat.observer.subject.SubjectVendor;
import csui.serbagunabot.coffeebot.chat.orderstates.*;
import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import lombok.Getter;


@Getter
public class MakingOrderCoffeeObserver implements Observer {

    private OrderState currentState;
    private StartOrderState startOrderState;
    private NotingOrderOrderState notingOrderOrderState;
    private PreparingIngredientsOrderState preparingIngredientsOrderState;
    private MakingCoffeeOrderState makingCoffeeOrderState;
    private AddingToppingOrderState addingToppingOrderState;

    private CoffeeBotUser coffeeBotUser;
    private Subject subjectVendor;

    /**
     * MakingOrderCoffeeObserver constructor.
     */
    public MakingOrderCoffeeObserver(CoffeeBotUser coffeeBotUser) {
        this.addingToppingOrderState = new AddingToppingOrderState(this, null);
        this.makingCoffeeOrderState = new MakingCoffeeOrderState(
                this, this.addingToppingOrderState
        );
        this.preparingIngredientsOrderState = new PreparingIngredientsOrderState(
                this, this.makingCoffeeOrderState
        );
        this.notingOrderOrderState = new NotingOrderOrderState(
                this, this.preparingIngredientsOrderState
        );
        this.startOrderState = new StartOrderState(this, this.notingOrderOrderState);

        this.currentState = this.startOrderState;

        this.coffeeBotUser = coffeeBotUser;
        this.subjectVendor = new SubjectVendor(this);
    }

    @Override
    public void startSubjectProcess() {
        this.subjectVendor.startProcess();
    }

    @Override
    public void update() {
        OrderState nextState = this.currentState.getNextState();

        if (nextState != null) {
            setCurrentState(nextState);
            String currentStateMessage = "<BARU!> " + getCurrentStateMessage();
            this.coffeeBotUser.pushMessage(currentStateMessage);
            startSubjectProcess();
        } else {
            this.coffeeBotUser.setCurrentState(this.coffeeBotUser.getWaitingPickUpCoffeeBotState());
            String message = this.coffeeBotUser.getCurrentStateMessage();
            this.coffeeBotUser.pushMessage(message);
            this.coffeeBotUser.getMakingOrderCoffeeBotState().finishMakingOrder();
        }
    }

    @Override
    public void setCurrentState(OrderState orderState) {
        this.currentState = orderState;
    }

    @Override
    public OrderState getCurrentState() {
        return this.currentState;
    }

    @Override
    public String getCurrentStateMessage() {
        return this.currentState.getMessage();
    }

}
