package csui.serbagunabot.coffeebot.chat.observer.subject;

public interface Subject {

    void startProcess();

    void notifyObserver();

}
