package csui.serbagunabot.coffeebot.chat.observer;

import csui.serbagunabot.coffeebot.chat.orderstates.OrderState;

public interface Observer {

    void startSubjectProcess();

    void update();

    void setCurrentState(OrderState orderState);

    OrderState getCurrentState();

    String getCurrentStateMessage();

}
