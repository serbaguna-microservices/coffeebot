package csui.serbagunabot.coffeebot.chat.states;

import csui.serbagunabot.coffeebot.chat.observer.MakingOrderCoffeeObserver;
import csui.serbagunabot.coffeebot.chat.observer.Observer;
import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;

public class MakingOrderCoffeeBotState implements CoffeeBotState {

    private CoffeeBotUser coffeeBotUser;
    private Observer makingOrderCoffeeObserver;
    private static final String COMMAND_NOT_FOUND =
            "Perintah '%s' yang diberikan tidak dikenal oleh CoffeeBot.\n\n"
            + "[Anda sedang berada di bagian '%s']\n\n"
            + "Ketik '!coffee help' untuk melihat perintah-perintah yang tersedia.";
    private static final String AVAILABLE_COMMANDS =
            "Selama menunggu pesanan Anda dibuat, "
            + "Anda dapat mengetik salah satu perintah berikut: \n\n"
                    + "- '!coffee status' \n"
                    + "Melihat status pembuatan pesanan Anda sekarang.\n\n"
                    + "- '!coffee help' \n"
                    + "Menampilkan halaman bantuan.";

    public MakingOrderCoffeeBotState(CoffeeBotUser coffeeBotUser) {
        this.coffeeBotUser = coffeeBotUser;
    }

    @Override
    public String doCommand(String command, String parameter) {
        if (command.equalsIgnoreCase("status")) {
            return this.makingOrderCoffeeObserver.getCurrentStateMessage();
        } else if (command.equalsIgnoreCase("help")) {
            return "[HELP] \nAnda sedang berada di bagian pembuatan coffee. \n\n"
                    + AVAILABLE_COMMANDS;
        } else {
            return String.format(COMMAND_NOT_FOUND, command, getStateName());
        }
    }

    @Override
    public String getMessage() {
        this.startMakingOrder();
        return "Terima kasih telah memesan di CoffeeBot! \n\n"
                + "Pesanan Anda sedang dibuat!\n"
                + "Setiap perkembangan pembuatan pesanan akan kami beritahukan kepada Anda.\n"
                + "Mohon kesabarannya."
                + "\n\n------------------------------------\n\n"
                + AVAILABLE_COMMANDS;
    }

    @Override
    public String getStateName() {
        return "Pembuatan Kopi";
    }

    /**
     * Start the making order process.
     * State start from StartOrderState.
     */
    public void startMakingOrder() {
        this.makingOrderCoffeeObserver = new MakingOrderCoffeeObserver(coffeeBotUser);

        this.makingOrderCoffeeObserver.startSubjectProcess();
    }

    /**
     * Finish the making order process.
     * State ends after AddingToppingOrderState.
     * Push next state message.
     */
    public void finishMakingOrder() {
        this.makingOrderCoffeeObserver = null;
    }

    public Observer getMakingOrderCoffeeObserver() {
        return this.makingOrderCoffeeObserver;
    }
}
