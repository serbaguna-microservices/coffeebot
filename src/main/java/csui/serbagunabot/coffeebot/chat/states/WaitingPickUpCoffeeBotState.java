package csui.serbagunabot.coffeebot.chat.states;

import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;

public class WaitingPickUpCoffeeBotState implements CoffeeBotState {

    private CoffeeBotUser coffeeBotUser;
    private static final String COMMAND_NOT_FOUND =
            "Perintah '%s' yang diberikan tidak dikenal oleh CoffeeBot.\n\n"
            + "[Anda sedang berada di bagian '%s']\n\n"
            + "Ketik '!coffee help' untuk melihat perintah-perintah yang tersedia.";
    private static final String AVAILABLE_COMMANDS =
            "Ketik salah satu perintah berikut: \n\n"
            + "- '!coffee selesai' \n"
                    + "Jika anda sudah pick-up pesanan dan ingin menyelesaikan pembelian.\n\n"
            + "- '!coffee help' \n"
                    + "Untuk menampilkan halaman bantuan.";

    public WaitingPickUpCoffeeBotState(CoffeeBotUser coffeeBotUser) {
        this.coffeeBotUser = coffeeBotUser;
    }

    @Override
    public String doCommand(String command, String parameter) {
        if (command.equalsIgnoreCase("selesai")) {
            this.coffeeBotUser.clearKeranjang();
            this.coffeeBotUser.setCurrentState(this.coffeeBotUser.getHomeCoffeeBotState());
            return "Pemesanan Anda telah selesai. \nTerima kasih banyak, selamat menikmati.";
        } else if (command.equalsIgnoreCase("help")) {
            return "[HELP] \nAnda sedang berada di "
                    + "bagian menunggu pick-up dan penyelesaian pemesanan. \n\n"
                    + AVAILABLE_COMMANDS;
        } else {
            return String.format(COMMAND_NOT_FOUND, command, getStateName());
        }
    }

    @Override
    public String getMessage() {
        return "[PESANANMU SUDAH SELESAI DIBUAT] \n\n"
                + "Pesanan Anda telah selesai dibuat, Anda dapat pick-up pesanan Anda. \n\n"
                + "Jangan lupa bawa uang yang cukup!\n"
                + "> Total Harga: Rp " + this.coffeeBotUser.getTotalPriceInKeranjang()
                + "\n\n" + AVAILABLE_COMMANDS;
    }

    @Override
    public String getStateName() {
        return "Menunggu Pick-Up dan Penyelesaian Pemesanan";
    }
}
