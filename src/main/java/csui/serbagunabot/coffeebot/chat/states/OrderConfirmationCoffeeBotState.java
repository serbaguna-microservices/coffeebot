package csui.serbagunabot.coffeebot.chat.states;

import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;

public class OrderConfirmationCoffeeBotState implements CoffeeBotState {

    private CoffeeBotUser coffeeBotUser;
    private static final String COMMAND_NOT_FOUND =
            "Perintah '%s' yang diberikan tidak dikenal oleh CoffeeBot.\n\n"
            + "[Anda sedang berada di bagian '%s']\n\n"
            + "Ketik '!coffee help' untuk melihat perintah-perintah yang tersedia.";
    private static final String AVAILABLE_COMMANDS =
            "Ketik salah satu perintah berikut: \n\n"
            + "- '!coffee konfirmasi' \n"
                    + "Untuk mengkonfirmasi pesanan.\n\n"
            + "- '!coffee kembali' \n"
                    + "Jika ingin kembali.\n\n"
            + "- '!coffee help' \n"
                    + "Untuk menampilkan halaman bantuan.";

    public OrderConfirmationCoffeeBotState(CoffeeBotUser coffeeBotUser) {
        this.coffeeBotUser = coffeeBotUser;
    }

    @Override
    public String doCommand(String command, String parameter) {
        if (command.equalsIgnoreCase("konfirmasi")) {
            this.coffeeBotUser.setCurrentState(this.coffeeBotUser.getMakingOrderCoffeeBotState());
            return "" + this.coffeeBotUser.getCurrentStateMessage();
        } else if (command.equalsIgnoreCase("kembali")) {
            this.coffeeBotUser.setCurrentState(this.coffeeBotUser.getHomeCoffeeBotState());
            return this.coffeeBotUser.getCurrentStateMessage();
        } else if (command.equalsIgnoreCase("help")) {
            return "[HELP] \nAnda sedang berada di bagian konfirmasi pembelian. \n\n"
                    + AVAILABLE_COMMANDS;
        } else {
            return String.format(COMMAND_NOT_FOUND, command, getStateName());
        }
    }

    @Override
    public String getMessage() {
        return "Apakah Anda sudah yakin untuk membeli item-item ini?\n\n"
                + coffeeBotUser.getAllItemInKeranjang() + "\n"
                + "Total Harga: Rp "
                + coffeeBotUser.getTotalPriceInKeranjang()
                + "\n\n------------------------------------\n\n"
                + AVAILABLE_COMMANDS;
    }

    @Override
    public String getStateName() {
        return "Konfirmasi Pembelian";
    }
}
