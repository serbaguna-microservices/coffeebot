package csui.serbagunabot.coffeebot.chat.states;

import csui.serbagunabot.coffeebot.model.coffee.Coffee;
import csui.serbagunabot.coffeebot.model.topping.Topping;
import csui.serbagunabot.coffeebot.model.topping.ToppingDecorator;
import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import java.util.List;


public class AddToppingCoffeeBotState implements CoffeeBotState {

    private CoffeeBotUser coffeeBotUser;
    private static final String COMMAND_NOT_FOUND =
            "Perintah '%s' yang diberikan tidak dikenal oleh CoffeeBot.\n\n"
            + "[Anda sedang berada di bagian '%s']\n\n"
            + "Ketik '!coffee help' untuk melihat perintah-perintah yang tersedia.";
    private static final String AVAILABLE_COMMANDS =
            "Ketik salah satu perintah berikut: \n\n"
            + "- '!coffee topping topping1 topping2' \n"
            + "Menambahkan topping ke coffee "
            + "(dapat menambahkan topping dengan jumlah lebih dari dua).\n\n"
            + "- '!coffee kembali' \n"
            + "Jika tidak ingin menambahkan topping.\n\n"
            + "- '!coffee help' \n"
            + "Menampilkan halaman bantuan.";

    public AddToppingCoffeeBotState(CoffeeBotUser coffeeBotUser) {
        this.coffeeBotUser = coffeeBotUser;
    }

    @Override
    public String doCommand(String command, String parameter) {
        if (command.equalsIgnoreCase("topping")) {
            Coffee coffeeAdded = coffeeBotUser.popLastAddedItemToKeranjang();
            List<Topping> allowedToppings = coffeeAdded.getAllowedToppings();
            String[] parameters = parameter.split(" ");

            StringBuilder response = new StringBuilder();
            response.append("Anda telah menambahkan topping-topping berikut ke ")
                    .append(coffeeAdded.getDescription())
                    .append(":\n\n");

            for (String namaTopping : parameters) {
                Topping topping = allowedToppings.stream()
                        .filter(o -> o.getDescription().equalsIgnoreCase(namaTopping))
                        .findFirst().orElse(null);
                if (topping != null) {
                    coffeeAdded = new ToppingDecorator(coffeeAdded, topping);
                    response.append("- ")
                            .append(topping.getDescription())
                            .append(" (Rp ")
                            .append(topping.cost())
                            .append(")")
                            .append("\n");
                } else {
                    response.append("(Topping '").append(namaTopping)
                            .append("' tidak ada dan gagal ditambahkan)\n");
                }
            }
            this.coffeeBotUser.addItemToKeranjang(coffeeAdded);
            this.coffeeBotUser.setCurrentState(this.coffeeBotUser.getHomeCoffeeBotState());
            return response.toString();
        } else if (command.equalsIgnoreCase("kembali")) {
            Coffee addedCoffee = this.coffeeBotUser.getLastAddedItemToKeranjang();

            this.coffeeBotUser.setCurrentState(this.coffeeBotUser.getHomeCoffeeBotState());
            return "Anda tidak menambahkan topping ke item " + addedCoffee.getDescription();
        } else if (command.equalsIgnoreCase("help")) {
            return "[HELP] \nAnda sedang berada di bagian pemilihan topping. \n\n"
                    + AVAILABLE_COMMANDS;
        } else {
            return String.format(COMMAND_NOT_FOUND, command, getStateName());
        }
    }

    @Override
    public String getMessage() {
        Coffee coffeeAdded = coffeeBotUser.getLastAddedItemToKeranjang();
        StringBuilder res = new StringBuilder();

        res.append("Anda telah menambahkan ")
                .append(coffeeAdded.getDescription())
                .append(" ke keranjang Anda!\n\n")
                .append(coffeeAdded.getAllowedToppingsAsString());
        res.append("\n\n------------------------------------\n\n")
                .append(AVAILABLE_COMMANDS);
        return res.toString();
    }

    @Override
    public String getStateName() {
        return "Pemilihan Topping";
    }
}
