package csui.serbagunabot.coffeebot.chat.states;

public interface CoffeeBotState {

    String doCommand(String command, String parameter);

    String getMessage();

    String getStateName();

}
