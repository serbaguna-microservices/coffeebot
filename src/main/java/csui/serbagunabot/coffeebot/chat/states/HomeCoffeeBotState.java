package csui.serbagunabot.coffeebot.chat.states;


import csui.serbagunabot.coffeebot.model.coffee.Coffee;
import csui.serbagunabot.coffeebot.model.coffee.ConcreteCoffee;
import csui.serbagunabot.coffeebot.model.topping.Topping;
import csui.serbagunabot.coffeebot.model.topping.ToppingDecorator;
import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import csui.serbagunabot.coffeebot.model.vendor.Vendor;
import csui.serbagunabot.coffeebot.repository.VendorRepository;
import csui.serbagunabot.coffeebot.repository.VendorRepositoryImpl;
import java.util.List;
import java.util.Set;


public class HomeCoffeeBotState implements CoffeeBotState {

    private VendorRepository vendorRepository = VendorRepositoryImpl.getUniqueVendorRepository();

    private CoffeeBotUser coffeeBotUser;
    private static final String COMMAND_NOT_FOUND =
            "Perintah '%s' yang diberikan tidak dikenal oleh CoffeeBot.\n\n"
            + "[Anda sedang berada di bagian '%s']\n\n"
            + "Ketik '!coffee help' untuk melihat perintah-perintah yang tersedia.";
    private static final String INVALID_PARAMETER =
            "Maaf, parameter '%s' yang anda berikan tidak tepat. Silakan dicoba lagi.";
    private static final String INVALID_NUMBER_FROM_KERANJANG =
            "Maaf, nomor urut '%s' "
            + "yang Anda berikan tidak ada di keranjang. \n\nSilakan dicoba lagi.";
    private static final String INVALID_VENDOR_ID =
            "Maaf, vendor id '%s' yang Anda berikan tidak dapat ditemukan.\n\n"
            + "Apakah Anda bermaksud untuk menuliskan vendor ini?\n"
            + "> %s";
    private static final String INVALID_COFFEE_NAME =
            "Maaf, minuman dengan nama '%s' yang Anda berikan tidak dapat ditemukan.\n\n"
            + "Apakah Anda bermaksud untuk menuliskan minuman ini?\n"
            + "> %s\n\n"
            + "Jika ya, ketik perintah berikut:\n- !coffee pesan %s %s";
    private static final String AVAILABLE_COMMANDS =
            "Ketik salah satu perintah berikut: \n\n"
            + "- '!coffee vendor' \n"
            + "Melihat list vendor.\n\n"
            + "- '!coffee menu id-vendor' \n"
            + "Melihat menu pada vendor yang dipilih.\n\n"
            + "- '!coffee pesan id-vendor nama-menu' \n"
            + "Menambahkan minuman ke keranjang.\n\n"
            + "- '!coffee list-topping no-urut' \n"
            + "Lihat list topping dari pesanan (dengan nomor urut di keranjang).\n\n"
            + "- '!coffee tambah-topping no-urut nama-topping' \n"
            + "Menambah topping dari pesanan.\n\n"
            + "- '!coffee hapus-topping no-urut nama-topping' \n"
            + "Menghapus topping dari pesanan.\n\n"
            + "- '!coffee hapus-pesanan no-urut' \n"
            + "Menghapus pesanan dari keranjang berdasarkan nomor urutnya.\n\n"
            + "- '!coffee beli' \n"
            + "Membeli minuman yang ada di keranjang.\n\n"
            + "- '!coffee keranjang' \n"
            + "Melihat keranjang belanja Anda (%s item).\n\n"
            + "- '!coffee hapus-keranjang' \n"
            + "Menghapus semua pesanan dalam keranjang belanja Anda.\n\n"
            + "- '!coffee help' \n"
            + "Menampilkan halaman bantuan.";

    public HomeCoffeeBotState(CoffeeBotUser coffeeBotUser) {
        this.coffeeBotUser = coffeeBotUser;
    }

    @Override
    public String doCommand(String command, String parameter) {
        if (command.equalsIgnoreCase("")) {
            return this.coffeeBotUser.getCurrentStateMessage();
        } else if (command.equalsIgnoreCase("vendor")) {
            return handleVendor();
        } else if (command.equalsIgnoreCase("menu")) {
            return handleMenu(parameter);
        } else if (command.equalsIgnoreCase("pesan")) {
            return handlePesan(parameter);
        } else if (command.equalsIgnoreCase("list-topping")) {
            return handleListTopping(parameter);
        } else if (command.equalsIgnoreCase("tambah-topping")) {
            return handleTambahTopping(parameter);
        } else if (command.equalsIgnoreCase("hapus-topping")) {
            return handleHapusTopping(parameter);
        } else if (command.equalsIgnoreCase("hapus-pesanan")) {
            return handleHapusPesanan(parameter);
        } else if (command.equalsIgnoreCase("beli")) {
            return handleBeli();
        } else if (command.equalsIgnoreCase("keranjang")) {
            return this.coffeeBotUser.getAllItemInKeranjang()
                    + "\nTotal Harga: Rp "
                    + this.coffeeBotUser.getTotalPriceInKeranjang();
        } else if (command.equalsIgnoreCase("hapus-keranjang")) {
            this.coffeeBotUser.clearKeranjang();
            return "Semua pesanan di keranjang Anda telah dihapus.";
        } else if (command.equalsIgnoreCase("help")) {
            String res = "[HELP] \nAnda sedang berada di Halaman Depan CoffeeBot!\n\n"
                    + AVAILABLE_COMMANDS;
            res = String.format(res, String.valueOf(this.coffeeBotUser.getSizeOfKeranjang()));
            return res;
        } else {
            return String.format(COMMAND_NOT_FOUND, command, getStateName());
        }
    }

    private String handleVendor() {
        Set<String> setOfVendorId = vendorRepository.getAllVendorId();

        StringBuilder res = new StringBuilder();
        res.append("List Vendor CoffeeBot:\n\n");
        for (String vendorId : setOfVendorId) {
            String vendorDesc = vendorRepository.getVendorDescById(vendorId);
            res.append("- ").append(vendorDesc).append("\n");
        }
        return res.toString();
    }

    private String handleMenu(String parameter) {
        Vendor vendor = vendorRepository.getVendorById(parameter);
        if (vendor != null) {
            return vendor.getMenu();
        }
        return String.format(INVALID_VENDOR_ID, parameter,
                this.getVendorRecommendationFromInvalidParameter(parameter));
    }

    private String handlePesan(String parameter) {
        String[] parameters = parameter.split(" ", 2);

        Vendor vendor = vendorRepository.getVendorById(parameters[0]);
        if (vendor != null) {
            Coffee coffee = vendorRepository
                    .getCoffeeByVendorAndName(vendor, parameters[1]);
            if (coffee == null) {
                String recommendedCoffee =
                        this.getCoffeeRecommendationFromInvalidParameter(vendor, parameters[1]);
                return String.format(INVALID_COFFEE_NAME, parameters[1],
                        recommendedCoffee,
                        vendor.getId(),
                        recommendedCoffee);
            }

            String responseHandleCoffeeFromDifferentVendor =
                    handlePesanCoffeeFromDifferentVendor(coffee);
            if (!responseHandleCoffeeFromDifferentVendor.equals("")) {
                return responseHandleCoffeeFromDifferentVendor;
            }

            this.coffeeBotUser.addItemToKeranjang(coffee);
            this.coffeeBotUser.setCurrentState(
                    this.coffeeBotUser.getAddToppingCoffeeBotState()
            );
            return coffeeBotUser.getCurrentStateMessage();
        }
        return String.format(INVALID_VENDOR_ID, parameters[0],
                this.getVendorRecommendationFromInvalidParameter(parameters[0]));
    }

    private String handleListTopping(String parameter) {
        String[] parameters = parameter.split(" ");
        if (parameters.length == 1) {
            if (isNumberAsStringInKeranjang(parameters[0])) {
                int idx = Integer.parseInt(parameters[0]);
                Coffee coffee = this.coffeeBotUser.getItemInKeranjangByIdx(idx - 1);
                return "[List Topping menu '" + coffee.getDescription() + "']\n\n"
                        + coffee.getAllowedToppingsAsString();
            }
            return String.format(INVALID_NUMBER_FROM_KERANJANG, parameters[0]);
        }
        return String.format(INVALID_PARAMETER, parameter);
    }

    private String handleTambahTopping(String parameter) {
        String[] parameters = parameter.split(" ");
        if (parameters.length == 2) {
            if (isNumberAsStringInKeranjang(parameters[0])) {
                int idx = Integer.parseInt(parameters[0]);

                Coffee coffee = this.coffeeBotUser.getItemInKeranjangByIdx(idx - 1);
                Topping topping = coffee.getAllowedToppings().stream()
                        .filter(o -> o.getDescription().equalsIgnoreCase(parameters[1]))
                        .findFirst().orElse(null);

                if (topping != null) {
                    coffee = new ToppingDecorator(coffee, topping);
                    this.coffeeBotUser.setItemInKeranjangByIdx(idx - 1, coffee);

                    return "Anda telah menambahkan topping '"
                            + parameters[1] + "' ke pesanan Anda.\n"
                            + "Sekarang pesananmu menjadi:\n\n"
                            + coffee.getDescription();
                }
                return "Topping '" + parameters[1] + "' salah atau tidak ditemukan. "
                        + "Tidak ada perubahan pada pesanan Anda. Silakan coba lagi.";
            }
            return String.format(INVALID_NUMBER_FROM_KERANJANG, parameters[0]);
        }
        return String.format(INVALID_PARAMETER, parameter);
    }

    private String handleHapusTopping(String parameter) {
        String[] parameters = parameter.split(" ");
        if (parameters.length == 2) {
            if (isNumberAsStringInKeranjang(parameters[0])) {
                int idx = Integer.parseInt(parameters[0]);
                Coffee coffee = this.coffeeBotUser.getItemInKeranjangByIdx(idx - 1);
                Coffee pointer = coffee;

                String deleteToppingName = parameters[1];
                ToppingDecorator lastDecorator = null;
                boolean isTerhapus = false;

                while (!(coffee instanceof ConcreteCoffee)) {
                    ToppingDecorator decorator = (ToppingDecorator) coffee;
                    String decoratorsToppingName = decorator.getToppingDescription();

                    if (decoratorsToppingName.equalsIgnoreCase(deleteToppingName)) {
                        if (lastDecorator == null) {
                            pointer = decorator.getCoffee();
                        } else {
                            lastDecorator.setCoffee(decorator.getCoffee());
                        }
                        isTerhapus = true;
                        break;
                    }
                    coffee = decorator.getCoffee();
                    lastDecorator = decorator;
                }
                if (isTerhapus) {
                    this.coffeeBotUser.setItemInKeranjangByIdx(idx - 1, pointer);
                    return "Topping '" + deleteToppingName
                            + "' telah dihapus dari pesananmu. \n"
                            + "Sekarang pesananmu menjadi: \n\n"
                            + pointer.getDescription();
                } else {
                    return "Maaf, pesananmu tidak memiliki topping '" + deleteToppingName
                            + "'. \n\nTidak ada perubahan pada pesananmu.";
                }
            }
            return String.format(INVALID_NUMBER_FROM_KERANJANG, parameters[0]);
        }
        return String.format(INVALID_PARAMETER, parameter);
    }

    private String handleHapusPesanan(String parameter) {
        if (isNumberAsStringInKeranjang(parameter)) {
            int idx = Integer.parseInt(parameter);
            this.coffeeBotUser.removeItemFromKeranjangByIdx(idx - 1);
            return "Pesanan dengan nomor urut " + parameter
                    + " telah dihapus dari keranjang";
        }
        return String.format(INVALID_NUMBER_FROM_KERANJANG, parameter);
    }

    private String handleBeli() {
        if (this.coffeeBotUser.getSizeOfKeranjang() > 0) {
            this.coffeeBotUser.setCurrentState(
                    this.coffeeBotUser.getOrderConfirmationCoffeeBotState()
            );
            return this.coffeeBotUser.getCurrentStateMessage();
        }
        return this.coffeeBotUser.getAllItemInKeranjang();
    }

    private boolean isNumberAsStringInKeranjang(String numAsString) {
        if (numAsString.matches("^[0-9]*$")) {
            int numAsInt = Integer.parseInt(numAsString);
            return (numAsInt <= this.coffeeBotUser.getSizeOfKeranjang() && numAsInt > 0);
        }
        return false;
    }

    private String handlePesanCoffeeFromDifferentVendor(Coffee coffee) {
        if (this.coffeeBotUser.getSizeOfKeranjang() != 0) {
            Coffee lastAdded = this.coffeeBotUser.getLastAddedItemToKeranjang();
            if (!coffee.getIdVendor().equalsIgnoreCase(lastAdded.getIdVendor())) {
                return "Maaf, sudah ada pesanan dari vendor lain dengan id: '"
                        + lastAdded.getIdVendor() +  "' dalam keranjang Anda.\n\n"
                        + "Jika Anda ingin membeli minuman dari vendor "
                        + "dengan id: '" + coffee.getIdVendor() + "', "
                        + "mohon hapus semua minuman dalam keranjang "
                        + "Anda terlebih dahulu.\n\n"
                        + "Ketik '!coffee hapus-keranjang'";
            }
        }
        return "";
    }

    private String getVendorRecommendationFromInvalidParameter(String parameter) {
        String[] parameterChars = parameter.split("");
        int wordLength = parameterChars.length;
        int charDifference = -1;
        String result = "";

        Set<String> vendorIdList = vendorRepository.getAllVendorId();
        for (String vendorId : vendorIdList) {
            String[] vendorIdChars = vendorId.split("");

            if (vendorIdChars.length < wordLength) {
                wordLength = vendorIdChars.length;
            }

            int tempCharDiff = 0;
            for (int i = 0; i < wordLength; i++) {
                if (!parameterChars[i].equalsIgnoreCase(vendorIdChars[i])) {
                    tempCharDiff++;
                }
            }

            if (charDifference == -1) {
                charDifference = tempCharDiff;
                result = vendorId;
            } else {
                if (tempCharDiff < charDifference) {
                    charDifference = tempCharDiff;
                    result = vendorId;
                }
            }

            wordLength = parameterChars.length;
        }

        Vendor vendorResult = vendorRepository.getVendorById(result);
        return vendorResult.getDescription();
    }

    private String getCoffeeRecommendationFromInvalidParameter(Vendor vendor, String parameter) {
        String[] parameterChars = parameter.split("");
        int wordLength = parameterChars.length;
        int charDifference = -1;
        String result = "";

        List<Coffee> coffeeList = vendorRepository.getListOfCoffeeOfVendor(vendor);
        for (Coffee coffee : coffeeList) {
            String[] coffeeChars = coffee.getDescription().split("");

            if (coffeeChars.length < wordLength) {
                wordLength = coffeeChars.length;
            }

            int tempCharDiff = 0;
            for (int i = 0; i < wordLength; i++) {
                if (!parameterChars[i].equalsIgnoreCase(coffeeChars[i])) {
                    tempCharDiff++;
                }
            }

            if (charDifference == -1) {
                charDifference = tempCharDiff;
                result = coffee.getDescription();
            } else {
                if (tempCharDiff < charDifference) {
                    charDifference = tempCharDiff;
                    result = coffee.getDescription();
                }
            }

            wordLength = parameterChars.length;
        }

        return result;
    }

    @Override
    public String getMessage() {
        String res = "Selamat datang di CoffeeBot!\n\n"
                + AVAILABLE_COMMANDS;
        res = String.format(res, String.valueOf(this.coffeeBotUser.getSizeOfKeranjang()));
        return res;
    }

    @Override
    public String getStateName() {
        return "Halaman Depan CoffeeBot";
    }
}
