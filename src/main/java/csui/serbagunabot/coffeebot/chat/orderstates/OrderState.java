package csui.serbagunabot.coffeebot.chat.orderstates;

public interface OrderState {

    String getMessage();

    OrderState getNextState();

}
