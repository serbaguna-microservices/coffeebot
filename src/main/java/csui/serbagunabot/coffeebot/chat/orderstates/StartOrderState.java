package csui.serbagunabot.coffeebot.chat.orderstates;

import csui.serbagunabot.coffeebot.chat.observer.Observer;

public class StartOrderState implements OrderState {

    private Observer observer;
    private OrderState nextOrderState;

    public StartOrderState(Observer observer, OrderState nextOrderState) {
        this.observer = observer;
        this.nextOrderState = nextOrderState;
    }

    @Override
    public String getMessage() {
        return "No Message";
    }

    @Override
    public OrderState getNextState() {
        return this.nextOrderState;
    }
}
