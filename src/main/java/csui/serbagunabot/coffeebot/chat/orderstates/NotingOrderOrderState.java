package csui.serbagunabot.coffeebot.chat.orderstates;

import csui.serbagunabot.coffeebot.chat.observer.Observer;

public class NotingOrderOrderState implements OrderState {

    private Observer observer;
    private OrderState nextOrderState;

    public NotingOrderOrderState(Observer observer, OrderState nextOrderState) {
        this.observer = observer;
        this.nextOrderState = nextOrderState;
    }

    @Override
    public String getMessage() {
        return "[STATUS PESANAN] \n\n> Pencatatan Pesanan \n\n"
                + "Pesananmu sedang dicatat dan diproses!";
    }

    @Override
    public OrderState getNextState() {
        return this.nextOrderState;
    }
}
