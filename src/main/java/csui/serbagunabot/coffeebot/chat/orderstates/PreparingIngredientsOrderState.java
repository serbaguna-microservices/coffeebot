package csui.serbagunabot.coffeebot.chat.orderstates;

import csui.serbagunabot.coffeebot.chat.observer.Observer;

public class PreparingIngredientsOrderState implements OrderState {

    private Observer observer;
    private OrderState nextOrderState;

    public PreparingIngredientsOrderState(Observer observer, OrderState nextOrderState) {
        this.observer = observer;
        this.nextOrderState = nextOrderState;
    }

    @Override
    public String getMessage() {
        return "[STATUS PESANAN] \n\n> Penyiapan Bahan \n\n"
                + "Bahan-bahan untuk pesananmu sedang disiapkan!";
    }

    @Override
    public OrderState getNextState() {
        return this.nextOrderState;
    }
}
