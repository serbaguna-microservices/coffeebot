package csui.serbagunabot.coffeebot.chat.orderstates;

import csui.serbagunabot.coffeebot.chat.observer.Observer;

public class AddingToppingOrderState implements OrderState {

    private Observer observer;
    private OrderState nextOrderState;

    public AddingToppingOrderState(Observer observer, OrderState nextOrderState) {
        this.observer = observer;
        this.nextOrderState = nextOrderState;
    }

    @Override
    public String getMessage() {
        return "[STATUS PESANAN] \n\n> Penambahan Topping\n\n"
                + "Pesananmu sedang ditambahkan topping pilihanmu!";
    }

    @Override
    public OrderState getNextState() {
        return this.nextOrderState;
    }
}
