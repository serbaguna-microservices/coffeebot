package csui.serbagunabot.coffeebot.chat.orderstates;

import csui.serbagunabot.coffeebot.chat.observer.Observer;

public class MakingCoffeeOrderState implements OrderState {

    private Observer observer;
    private OrderState nextOrderState;

    public MakingCoffeeOrderState(Observer observer, OrderState nextOrderState) {
        this.observer = observer;
        this.nextOrderState = nextOrderState;
    }

    @Override
    public String getMessage() {
        return "[STATUS PESANAN] \n\n> Pembuatan Pesanan \n\n"
                + "Pesananmu sedang dibuatkan! ";
    }

    @Override
    public OrderState getNextState() {
        return this.nextOrderState;
    }
}
