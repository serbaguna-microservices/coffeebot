package csui.serbagunabot.coffeebot;

import csui.serbagunabot.coffeebot.model.coffee.Coffee;
import csui.serbagunabot.coffeebot.model.coffee.ConcreteCoffee;
import csui.serbagunabot.coffeebot.model.topping.Topping;
import csui.serbagunabot.coffeebot.model.vendor.Vendor;
import csui.serbagunabot.coffeebot.repository.CoffeeBotUserRepository;
import csui.serbagunabot.coffeebot.repository.VendorRepository;
import csui.serbagunabot.coffeebot.repository.VendorRepositoryImpl;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class CoffeeBotInitializer {

    @Autowired
    private CoffeeBotUserRepository coffeeBotUserRepository;

    private VendorRepository vendorRepository = VendorRepositoryImpl.getUniqueVendorRepository();

    /**
     * Initializer.
     */
    @PostConstruct
    public void init() {

        Vendor vendor1 = new Vendor("Starbucks", "Jakarta", "sbjkt");
        Vendor vendor2 = new Vendor("Kopi Nusantara", "Depok", "knd");
        Vendor vendor3 = new Vendor("Kopni", "Bandung", "kopb");
        Vendor vendor4 = new Vendor("Starbucks", "Bandung", "sbbdg");

        Coffee coffee1 = new ConcreteCoffee("Caffe Latte", 40000);
        Coffee coffee2 = new ConcreteCoffee("Dark Mocha", 50000);
        Coffee coffee3 = new ConcreteCoffee("Kopi Nusantara", 18000);
        Coffee coffee4 = new ConcreteCoffee("Es Kopi Susu", 18000);
        Coffee coffee5 = new ConcreteCoffee("Black Coffee", 20000);
        Coffee coffee6 = new ConcreteCoffee("Americano", 15000);
        Coffee coffee7 = new ConcreteCoffee("Frappuccino", 65000);
        Coffee coffee8 = new ConcreteCoffee("Cold Brew", 52000);
        Coffee coffee9 = new ConcreteCoffee("Ice Latte", 35000);
        Coffee coffee10 = new ConcreteCoffee("Cappucino", 46000);
        Coffee coffee11 = new ConcreteCoffee("Kopi Mantan", 18000);
        Coffee coffee12 = new ConcreteCoffee("Java Chip", 60000);

        vendorRepository.saveVendor(vendor1);
        vendorRepository.saveVendor(vendor2);
        vendorRepository.saveVendor(vendor3);
        vendorRepository.saveVendor(vendor4);

        vendorRepository.saveCoffeeToVendor(vendor1, coffee1);
        vendorRepository.saveCoffeeToVendor(vendor1, coffee2);
        vendorRepository.saveCoffeeToVendor(vendor1, coffee7);
        vendorRepository.saveCoffeeToVendor(vendor1, coffee8);
        vendorRepository.saveCoffeeToVendor(vendor1, coffee12);
        vendorRepository.saveCoffeeToVendor(vendor2, coffee3);
        vendorRepository.saveCoffeeToVendor(vendor2, coffee4);
        vendorRepository.saveCoffeeToVendor(vendor3, coffee5);
        vendorRepository.saveCoffeeToVendor(vendor3, coffee6);
        vendorRepository.saveCoffeeToVendor(vendor3, coffee11);
        vendorRepository.saveCoffeeToVendor(vendor4, coffee9);
        vendorRepository.saveCoffeeToVendor(vendor4, coffee10);

        Topping topping1 = new Topping("Susu", 5000);
        Topping topping2 = new Topping("Espresso-Shot", 5000);
        Topping topping3 = new Topping("Gula", 3000);
        Topping topping4 = new Topping("Oreo", 8000);
        Topping topping5 = new Topping("Regal", 7500);
        Topping topping6 = new Topping("Cokelat", 7500);
        Topping topping7 = new Topping("Madu", 15000);
        Topping topping8 = new Topping("Choco-Chip", 7500);
        Topping topping9 = new Topping("Gula-Merah", 4000);

        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor1, topping1);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor2, topping1);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor3, topping1);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor4, topping1);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor1, topping2);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor2, topping2);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor3, topping2);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor4, topping2);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor1, topping3);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor2, topping3);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor3, topping3);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor4, topping3);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor1, topping6);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor2, topping6);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor3, topping6);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor4, topping6);
        vendorRepository.saveToppingToAllCoffeeOnVendor(vendor1, topping8);
        vendorRepository.saveToppingToCoffee(coffee3, topping4);
        vendorRepository.saveToppingToCoffee(coffee3, topping5);
        vendorRepository.saveToppingToCoffee(coffee5, topping7);
        vendorRepository.saveToppingToCoffee(coffee6, topping7);
        vendorRepository.saveToppingToCoffee(coffee7, topping7);
        vendorRepository.saveToppingToCoffee(coffee9, topping7);
        vendorRepository.saveToppingToCoffee(coffee10, topping7);
        vendorRepository.saveToppingToCoffee(coffee11, topping7);
        vendorRepository.saveToppingToCoffee(coffee11, topping9);
    }

}
