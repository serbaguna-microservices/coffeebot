package csui.serbagunabot.coffeebot.repository;

import csui.serbagunabot.coffeebot.model.coffee.Coffee;
import csui.serbagunabot.coffeebot.model.topping.Topping;
import csui.serbagunabot.coffeebot.model.vendor.Vendor;
import java.util.*;


public class VendorRepositoryImpl implements VendorRepository {

    private static VendorRepositoryImpl uniqueVendorRepository = new VendorRepositoryImpl();

    private Map<String, Vendor> vendorMap = new HashMap<>();
    private Map<String, List<Coffee>> coffeeMap = new HashMap<>();

    private VendorRepositoryImpl() {}

    public static VendorRepositoryImpl getUniqueVendorRepository() {
        return uniqueVendorRepository;
    }

    @Override
    public Vendor getVendorById(String id) {
        return vendorMap.get(id);
    }

    @Override
    public Set<String> getAllVendorId() {
        return vendorMap.keySet();
    }

    @Override
    public String getVendorDescById(String id) {
        Vendor vendor = this.getVendorById(id);
        if (vendor != null) {
            return vendorMap.get(id).getDescription();
        }
        return null;
    }

    @Override
    public List<Coffee> getListOfCoffeeOfVendor(Vendor vendor) {
        String key = vendor.getId();
        return coffeeMap.get(key);
    }

    @Override
    public Coffee getCoffeeByVendorAndName(Vendor vendor, String name) {
        String key = vendor.getId();
        List<Coffee> listOfCoffee = coffeeMap.get(key);
        if (listOfCoffee != null) {
            return listOfCoffee.stream()
                    .filter(coffee -> name.equalsIgnoreCase(coffee.getDescription()))
                    .findFirst().orElse(null);
        } else {
            return null;
        }
    }

    @Override
    public void saveVendor(Vendor vendor) {
        String key = vendor.getId();
        vendorMap.put(key, vendor);
    }

    @Override
    public void saveCoffeeToVendor(Vendor vendor, Coffee coffee) {
        String key = vendor.getId();
        coffeeMap.computeIfAbsent(key, k -> new ArrayList<>()).add(coffee);
        coffee.setIdVendor(key);
    }

    @Override
    public void saveToppingToCoffee(Coffee coffee, Topping topping) {
        coffee.addToppingToAllowedToppings(topping);
    }

    @Override
    public void saveToppingToAllCoffeeOnVendor(Vendor vendor, Topping topping) {
        List<Coffee> listOfCoffeeOfVendor = this.getListOfCoffeeOfVendor(vendor);
        for (Coffee coffee : listOfCoffeeOfVendor) {
            coffee.addToppingToAllowedToppings(topping);
        }
    }
}
