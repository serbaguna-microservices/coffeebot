package csui.serbagunabot.coffeebot.repository;

import csui.serbagunabot.coffeebot.model.coffee.Coffee;
import csui.serbagunabot.coffeebot.model.topping.Topping;
import csui.serbagunabot.coffeebot.model.vendor.Vendor;
import java.util.List;
import java.util.Set;

public interface VendorRepository {

    Vendor getVendorById(String id);

    Coffee getCoffeeByVendorAndName(Vendor vendor, String name);

    List<Coffee> getListOfCoffeeOfVendor(Vendor vendor);

    Set<String> getAllVendorId();

    String getVendorDescById(String id);

    void saveVendor(Vendor vendor);

    void saveCoffeeToVendor(Vendor vendor, Coffee coffee);

    void saveToppingToCoffee(Coffee coffee, Topping topping);

    void saveToppingToAllCoffeeOnVendor(Vendor vendor, Topping topping);
}
