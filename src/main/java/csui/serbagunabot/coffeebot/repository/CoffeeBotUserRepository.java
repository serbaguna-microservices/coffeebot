package csui.serbagunabot.coffeebot.repository;

import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;

public interface CoffeeBotUserRepository {

    CoffeeBotUser findByUserId(String userId);

    CoffeeBotUser createUserFromId(String userId);
}
