package csui.serbagunabot.coffeebot.repository;

import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Repository;


@Repository
public class CoffeeBotUserRepositoryImpl implements CoffeeBotUserRepository {

    @Autowired
    private AutowireCapableBeanFactory autowireCapableBeanFactory;

    Map<String, CoffeeBotUser> userMap = new HashMap<>();

    @Override
    public CoffeeBotUser findByUserId(String userId) {
        return userMap.get(userId);
    }

    @Override
    public CoffeeBotUser createUserFromId(String userId) {
        CoffeeBotUser newUser = new CoffeeBotUser(userId);

        autowireCapableBeanFactory
                .autowireBeanProperties(newUser,
                        AutowireCapableBeanFactory.AUTOWIRE_AUTODETECT,
                        true);

        userMap.put(userId, newUser);
        return findByUserId(userId);
    }

}
