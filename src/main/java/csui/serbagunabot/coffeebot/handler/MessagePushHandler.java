package csui.serbagunabot.coffeebot.handler;

import csui.serbagunabot.coffeebot.model.line.LineRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessagePushHandler implements FeaturePushHandler {

    @Autowired
    CoffeeBotPushMessageFeign coffeeBotPushMessageFeign;

    /**
     * Push Message to be handled by feign.
     * @param message message sent to the user
     * @param userId userId of the receiver of push message
     */
    public void handlePushMessage(String message, String userId) {
        LineRequest lineRequest = new LineRequest(message, userId);

        coffeeBotPushMessageFeign.handleCoffeebotMessage(lineRequest);
    }

}
