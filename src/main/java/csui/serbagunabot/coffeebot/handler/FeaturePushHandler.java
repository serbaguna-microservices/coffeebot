package csui.serbagunabot.coffeebot.handler;

public interface FeaturePushHandler {
    /**
     * Handles push message.
     * @param message message sent to the user
     * @param userId userId of the receiver of push message
     */
    public void handlePushMessage(String message, String userId);
}
