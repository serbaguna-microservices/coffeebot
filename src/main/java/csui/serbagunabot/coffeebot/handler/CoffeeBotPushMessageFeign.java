package csui.serbagunabot.coffeebot.handler;

import csui.serbagunabot.coffeebot.model.line.LineRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(name = "coffeebotpushmessage", url = "${serbaguna.message-proxy}")
public interface CoffeeBotPushMessageFeign {

    @PostMapping("/")
    void handleCoffeebotMessage(@RequestBody LineRequest lineRequest);

}
