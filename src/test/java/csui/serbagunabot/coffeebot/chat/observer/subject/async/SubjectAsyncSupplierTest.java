package csui.serbagunabot.coffeebot.chat.observer.subject.async;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SubjectAsyncSupplierTest {

    private SubjectAsyncSupplier subjectAsyncSupplier;

    @BeforeEach
    public void setUp() {
        subjectAsyncSupplier = new SubjectAsyncSupplier();
    }

    @Test
    void testSubjectAsyncSupplierGetNotInterrupted() {

        String res = subjectAsyncSupplier.get();

        Assertions.assertFalse(Thread.interrupted());
        Assertions.assertEquals("Done", res);
    }

    @Test
    void testSubjectAsyncSupplierGetInterrupted() {

        String res = subjectAsyncSupplier.get();

        Thread.currentThread().interrupt();

        Assertions.assertTrue(Thread.interrupted());
    }

}