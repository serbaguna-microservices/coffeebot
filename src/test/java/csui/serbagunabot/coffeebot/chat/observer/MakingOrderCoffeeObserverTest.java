package csui.serbagunabot.coffeebot.chat.observer;

import static org.mockito.Mockito.*;

import csui.serbagunabot.coffeebot.chat.observer.subject.Subject;
import csui.serbagunabot.coffeebot.chat.observer.subject.SubjectVendor;
import csui.serbagunabot.coffeebot.chat.orderstates.*;
import csui.serbagunabot.coffeebot.chat.states.MakingOrderCoffeeBotState;
import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class MakingOrderCoffeeObserverTest {

    @Mock
    private NotingOrderOrderState notingOrderOrderState;

    @Mock
    private PreparingIngredientsOrderState preparingIngredientsOrderState;

    @Mock
    private MakingCoffeeOrderState makingCoffeeOrderState;

    @Mock
    private AddingToppingOrderState addingToppingOrderState;

    @Mock
    private MakingOrderCoffeeBotState makingOrderCoffeeBotState;

    @Mock
    private CoffeeBotUser coffeeBotUser;

    @Mock
    private Subject subject;

    private MakingOrderCoffeeObserver makingOrderCoffeeObserver;

    @BeforeEach
    public void setUo() {
        this.makingOrderCoffeeObserver = new MakingOrderCoffeeObserver(coffeeBotUser);
    }

    @Test
    void testCoffeeBotGetter() {
        Assertions.assertEquals(coffeeBotUser, makingOrderCoffeeObserver.getCoffeeBotUser());
    }

    @Test
    void testSubjectVendorGetter() {
        Assertions.assertTrue(
                makingOrderCoffeeObserver.getSubjectVendor() instanceof SubjectVendor
        );
    }

    @Test
    void testInitialStateIsNotingOrderOrderState() {
        Assertions.assertTrue(
                makingOrderCoffeeObserver.getCurrentState() instanceof StartOrderState
        );
    }

    @Test
    void testSetCurrentStates() {
        Assertions.assertTrue(
                makingOrderCoffeeObserver.getCurrentState() instanceof StartOrderState
        );
        makingOrderCoffeeObserver.setCurrentState(
                makingOrderCoffeeObserver.getAddingToppingOrderState()
        );
        Assertions.assertTrue(
                makingOrderCoffeeObserver.getCurrentState() instanceof AddingToppingOrderState
        );
    }

    @Test
    void testGetCurrentStateMessage() {
        String res = makingOrderCoffeeObserver.getCurrentStateMessage();
        String expected = makingOrderCoffeeObserver.getCurrentState().getMessage();

        Assertions.assertEquals(expected, res);
    }

    @Test
    void testUpdateMoveObserverToNextState() {
        doNothing().when(coffeeBotUser).pushMessage(anyString());

        Assertions.assertTrue(
                makingOrderCoffeeObserver.getCurrentState() instanceof StartOrderState
        );
        makingOrderCoffeeObserver.update();
        Assertions.assertEquals(
                makingOrderCoffeeObserver.getNotingOrderOrderState(),
                makingOrderCoffeeObserver.getCurrentState()
        );
        makingOrderCoffeeObserver.update();
        Assertions.assertEquals(
                makingOrderCoffeeObserver.getPreparingIngredientsOrderState(),
                makingOrderCoffeeObserver.getCurrentState()
        );
        makingOrderCoffeeObserver.update();
        Assertions.assertEquals(
                makingOrderCoffeeObserver.getMakingCoffeeOrderState(),
                makingOrderCoffeeObserver.getCurrentState()
        );
        makingOrderCoffeeObserver.update();
        Assertions.assertEquals(
                makingOrderCoffeeObserver.getAddingToppingOrderState(),
                makingOrderCoffeeObserver.getCurrentState()
        );
    }

    @Test
    void testUpdateWhenInAddingToppingStateCallsMakingOrderCoffeeBotStateFinishMakingOrder() {
        doNothing().when(coffeeBotUser).pushMessage(anyString());
        when(coffeeBotUser.getMakingOrderCoffeeBotState()).thenReturn(makingOrderCoffeeBotState);

        Assertions.assertTrue(
                makingOrderCoffeeObserver.getCurrentState() instanceof StartOrderState
        );
        makingOrderCoffeeObserver.update();
        makingOrderCoffeeObserver.update();
        makingOrderCoffeeObserver.update();
        makingOrderCoffeeObserver.update();
        Assertions.assertTrue(
                makingOrderCoffeeObserver.getCurrentState() instanceof AddingToppingOrderState
        );
        makingOrderCoffeeObserver.update();
        verify(makingOrderCoffeeBotState, times(1)).finishMakingOrder();
    }
}