package csui.serbagunabot.coffeebot.chat.observer.subject;

import static org.mockito.Mockito.*;

import csui.serbagunabot.coffeebot.chat.observer.Observer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class SubjectVendorTest {

    @Mock
    Observer observer;

    private SubjectVendor subjectVendor;

    @BeforeEach
    void setUp() {
        subjectVendor = new SubjectVendor(observer);
    }

    @Test
    void testStartProcess() {
        Assertions.assertDoesNotThrow(() -> {
            subjectVendor.startProcess();
        });
    }

    @Test
    void testNotifyObserverShouldCallObserverUpdate() {
        subjectVendor.notifyObserver();
        verify(observer, times(1)).update();
    }
}