package csui.serbagunabot.coffeebot.chat.orderstates;


import csui.serbagunabot.coffeebot.chat.observer.MakingOrderCoffeeObserver;
import csui.serbagunabot.coffeebot.chat.states.MakingOrderCoffeeBotState;
import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

class AddingToppingOrderStateTest {

    @Mock
    private MakingOrderCoffeeBotState makingOrderCoffeeBotState;

    @Mock
    CoffeeBotUser coffeeBotUser;

    private MakingOrderCoffeeObserver makingOrderCoffeeObserver;
    private static final String MESSAGE = "[STATUS PESANAN] \n\n> Penambahan Topping\n\n"
            + "Pesananmu sedang ditambahkan topping pilihanmu!";

    @BeforeEach
    void setUp() {
        makingOrderCoffeeObserver = new MakingOrderCoffeeObserver(coffeeBotUser);
        makingOrderCoffeeObserver.setCurrentState(
                makingOrderCoffeeObserver.getAddingToppingOrderState()
        );
    }

    @Test
    void testGetMessage() {
        String res = makingOrderCoffeeObserver.getCurrentState().getMessage();
        Assertions.assertEquals(MESSAGE, res);
    }

    @Test
    void testGetNextStateShouldReturnNull() {
        OrderState nextState = makingOrderCoffeeObserver
                .getCurrentState()
                .getNextState();
        Assertions.assertNull(nextState);
    }
}