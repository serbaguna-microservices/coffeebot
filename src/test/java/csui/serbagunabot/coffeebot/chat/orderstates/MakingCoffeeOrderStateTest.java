package csui.serbagunabot.coffeebot.chat.orderstates;

import csui.serbagunabot.coffeebot.chat.observer.MakingOrderCoffeeObserver;
import csui.serbagunabot.coffeebot.chat.states.MakingOrderCoffeeBotState;
import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


class MakingCoffeeOrderStateTest {

    @Mock
    private MakingOrderCoffeeBotState makingOrderCoffeeBotState;

    @Mock
    CoffeeBotUser coffeeBotUser;

    private MakingOrderCoffeeObserver makingOrderCoffeeObserver;
    private static final String MESSAGE = "[STATUS PESANAN] \n\n> Pembuatan Pesanan \n\n"
                + "Pesananmu sedang dibuatkan! ";

    @BeforeEach
    void setUp() {
        makingOrderCoffeeObserver = new MakingOrderCoffeeObserver(coffeeBotUser);
        makingOrderCoffeeObserver.setCurrentState(
                makingOrderCoffeeObserver.getMakingCoffeeOrderState()
        );
    }

    @Test
    void testGetMessage() {
        String res = makingOrderCoffeeObserver.getCurrentState().getMessage();
        Assertions.assertEquals(MESSAGE, res);
    }

    @Test
    void testGetNextStateShouldReturnAddingToppingOrderState() {
        OrderState nextState = makingOrderCoffeeObserver
                .getCurrentState()
                .getNextState();
        Assertions.assertEquals(
                makingOrderCoffeeObserver.getAddingToppingOrderState(), nextState
        );
    }
}