package csui.serbagunabot.coffeebot.chat.states;

import csui.serbagunabot.coffeebot.model.coffee.Coffee;
import csui.serbagunabot.coffeebot.model.coffee.ConcreteCoffee;
import csui.serbagunabot.coffeebot.model.topping.Topping;
import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import csui.serbagunabot.coffeebot.model.vendor.Vendor;
import csui.serbagunabot.coffeebot.repository.VendorRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


class AddToppingCoffeeBotStateTest {

    private VendorRepositoryImpl vendorRepository;

    private CoffeeBotUser coffeeBotUser;
    private Vendor vendor;
    private Coffee coffee;
    private Topping topping;

    private static final String USER_ID = "azricot";
    private static final String VENDOR_NAME = "Starbucks";
    private static final String VENDOR_LOCATION = "Jakarta";
    private static final String VENDOR_ID = "sbjkt";
    private static final String COFFEE_NAME = "Caffe Latte";
    private static final int COFFEE_PRICE = 50000;
    private static final String TOPPING_NAME = "Boba";
    private static final int TOPPING_PRICE = 5000;

    @BeforeEach
    public void setUp() {
        coffeeBotUser = new CoffeeBotUser(USER_ID);
        coffeeBotUser.setCurrentState(this.coffeeBotUser.getAddToppingCoffeeBotState());
        vendorRepository = VendorRepositoryImpl.getUniqueVendorRepository();

        vendor = new Vendor(VENDOR_NAME, VENDOR_LOCATION, VENDOR_ID);
        coffee = new ConcreteCoffee(COFFEE_NAME, COFFEE_PRICE);

        vendorRepository.saveVendor(vendor);
        vendorRepository.saveCoffeeToVendor(vendor, coffee);

        coffeeBotUser.addItemToKeranjang(coffee);

        topping = new Topping(TOPPING_NAME, TOPPING_PRICE);
        vendorRepository.saveToppingToCoffee(coffee, topping);
    }

    @ParameterizedTest
    @CsvSource({
            "'topping', 'Boba', ''",
            "'topping', 'tanpa-topping', ''",
            "'help', '', 'Anda sedang berada di bagian pemilihan topping'",
            "'BLABLABLABLAAAAA', '', 'yang diberikan tidak dikenal oleh CoffeeBot.'",
    })
    void testAddToppingStateDoCommandReturnCorrectResponse(String cmd, String param, String exp) {
        String res = this.coffeeBotUser.getCurrentState().doCommand(cmd, param);
        Assertions.assertTrue(res.contains(exp));
    }

    @Test
    void testAddToppingStateDoCommandToppingToppingsChangeStateToHomeState() {
        String command = "topping";
        String parameter = "Boba";
        this.coffeeBotUser.getCurrentState().doCommand(command, parameter);
        Assertions.assertEquals(this.coffeeBotUser.getCurrentState(),
                this.coffeeBotUser.getHomeCoffeeBotState());
    }

    @Test
    void testAddToppingStateDoCommandHelpDoesNotChangeState() {
        String command = "help";
        String parameter = "";
        this.coffeeBotUser.getCurrentState().doCommand(command, parameter);
        Assertions.assertEquals(this.coffeeBotUser.getCurrentState(),
                this.coffeeBotUser.getAddToppingCoffeeBotState());
    }

    @Test
    void testAddToppingStateDoCommandKembaliChangeStateToHomeState() {
        String command = "kembali";
        String parameter = "";
        this.coffeeBotUser.getCurrentState().doCommand(command, parameter);
        Assertions.assertEquals(this.coffeeBotUser.getCurrentState(),
                this.coffeeBotUser.getHomeCoffeeBotState());
    }

    @Test
    void testAddToppingStateGetMessage() {
        String res = this.coffeeBotUser.getCurrentState().getMessage();
        Assertions.assertTrue(res.contains("Anda telah menambahkan")
                && res.contains("ke keranjang Anda!"));
    }
}