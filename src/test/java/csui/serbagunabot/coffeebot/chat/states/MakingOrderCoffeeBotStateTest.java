package csui.serbagunabot.coffeebot.chat.states;

import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


class MakingOrderCoffeeBotStateTest {

    private CoffeeBotUser coffeeBotUser;

    private static final String USER_ID = "azricot";

    @BeforeEach
    public void setUp() {
        coffeeBotUser = new CoffeeBotUser(USER_ID);
        coffeeBotUser.setCurrentState(this.coffeeBotUser.getMakingOrderCoffeeBotState());
        coffeeBotUser.getMakingOrderCoffeeBotState().startMakingOrder();
    }

    @ParameterizedTest
    @CsvSource({
            "'status', '', 'No Message'",
            "'help', '', 'Anda sedang berada di bagian pembuatan coffee'",
            "'BLABLABLABLAAAAA', '', 'yang diberikan tidak dikenal oleh CoffeeBot.'",
    })
    void testMakingOrderStateDoCommandReturnCorrectResponse(String cmd, String param, String exp) {
        String res = this.coffeeBotUser.getCurrentState().doCommand(cmd, param);
        Assertions.assertTrue(res.contains(exp));
    }

    @Test
    void testMakingOrderStateDoCommandHelpDoesNotChangeState() {
        String command = "help";
        String parameter = "";
        this.coffeeBotUser.getCurrentState().doCommand(command, parameter);
        Assertions.assertEquals(this.coffeeBotUser.getCurrentState(),
                this.coffeeBotUser.getMakingOrderCoffeeBotState());
    }

    @Test
    void testMakingOrderStateGetMessage() {
        String res = this.coffeeBotUser.getCurrentState().getMessage();
        Assertions.assertTrue(res.contains("Terima kasih telah memesan di CoffeeBot!"));
    }

    @Test
    void testFinishMakingOrderShouldMakeMakingOrderStateNull() {
        coffeeBotUser.getMakingOrderCoffeeBotState().startMakingOrder();
        Assertions.assertNotNull(
                coffeeBotUser
                        .getMakingOrderCoffeeBotState()
                        .getMakingOrderCoffeeObserver()
        );
        coffeeBotUser.getMakingOrderCoffeeBotState().finishMakingOrder();
        Assertions.assertNull(
                coffeeBotUser
                        .getMakingOrderCoffeeBotState()
                        .getMakingOrderCoffeeObserver()
        );
    }
}