package csui.serbagunabot.coffeebot.chat.states;

import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


class WaitingPickUpCoffeeBotStateTest {

    private CoffeeBotUser coffeeBotUser;

    private static final String USER_ID = "azricot";

    @BeforeEach
    public void setUp() {
        coffeeBotUser = new CoffeeBotUser(USER_ID);
        coffeeBotUser.setCurrentState(this.coffeeBotUser.getWaitingPickUpCoffeeBotState());
    }

    @ParameterizedTest
    @CsvSource({
            "'selesai', '', 'Pemesanan Anda telah selesai.'",
            "'help', '', 'Anda sedang berada di bagian menunggu pick-up'",
            "'BLABLABLABLAAAAA', '', 'yang diberikan tidak dikenal oleh CoffeeBot.'",
    })
    void testWaitPickUpStateDoCommandReturnCorrectResponse(String cmd, String param, String exp) {
        String res = this.coffeeBotUser.getCurrentState().doCommand(cmd, param);
        Assertions.assertTrue(res.contains(exp));
    }

    @Test
    void testWaitingPickUpStateDoCommandSelesaiChangeStateToWaitingPickUpCoffeeBotState() {
        String command = "selesai";
        String parameter = "";
        this.coffeeBotUser.getCurrentState().doCommand(command, parameter);
        Assertions.assertEquals(this.coffeeBotUser.getCurrentState(),
                this.coffeeBotUser.getHomeCoffeeBotState());
    }

    @Test
    void testWaitingPickUpStateDoCommandHelpDoesNotChangeState() {
        String command = "help";
        String parameter = "";
        this.coffeeBotUser.getCurrentState().doCommand(command, parameter);
        Assertions.assertEquals(this.coffeeBotUser.getCurrentState(),
                this.coffeeBotUser.getWaitingPickUpCoffeeBotState());
    }

    @Test
    void testWaitingPickUpStateGetMessage() {
        String res = this.coffeeBotUser.getCurrentState().getMessage();
        Assertions.assertTrue(
                res.contains("Pesanan Anda telah selesai dibuat, Anda dapat pick-up pesanan Anda")
        );
    }
}