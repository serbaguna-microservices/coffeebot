package csui.serbagunabot.coffeebot.chat.states;

import csui.serbagunabot.coffeebot.model.coffee.Coffee;
import csui.serbagunabot.coffeebot.model.coffee.ConcreteCoffee;
import csui.serbagunabot.coffeebot.model.topping.Topping;
import csui.serbagunabot.coffeebot.model.topping.ToppingDecorator;
import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import csui.serbagunabot.coffeebot.model.vendor.Vendor;
import csui.serbagunabot.coffeebot.repository.VendorRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


class HomeCoffeeBotStateTest {

    private VendorRepositoryImpl vendorRepository;

    private CoffeeBotUser coffeeBotUser;
    private Vendor vendor;
    private Vendor vendor2;
    private Coffee coffee;
    private Coffee coffee3;
    private Coffee coffee4;
    private Topping topping1;
    private Topping topping2;

    private static final String USER_ID = "azricot";

    private static final String VENDOR_NAME = "Starbucks";
    private static final String VENDOR_LOCATION = "Jakarta";
    private static final String VENDOR_ID = "sbjkt";
    private static final String COFFEE_NAME = "Caffe Latte";
    private static final int COFFEE_PRICE = 50000;
    private static final String VENDOR2_NAME = "Starbucks";
    private static final String VENDOR2_LOCATION = "Bandung";
    private static final String VENDOR2_ID = "sbbdg";
    private static final String COFFEE2_NAME = "Iced Latte";
    private static final int COFFEE2_PRICE = 40000;
    private static final String COFFEE3_NAME = "Black Coffee";
    private static final int COFFEE3_PRICE = 20000;


    private static final String TOPPING_NAME1 = "Boba";
    private static final int TOPPING_PRICE1 = 5000;
    private static final String TOPPING_NAME2 = "Gula";
    private static final int TOPPING_PRICE2 = 5000;


    @BeforeEach
    public void setUp() {
        coffeeBotUser = new CoffeeBotUser(USER_ID);
        coffeeBotUser.setCurrentState(this.coffeeBotUser.getHomeCoffeeBotState());
        vendorRepository = VendorRepositoryImpl.getUniqueVendorRepository();

        vendor = new Vendor(VENDOR_NAME, VENDOR_LOCATION, VENDOR_ID);
        coffee = new ConcreteCoffee(COFFEE_NAME, COFFEE_PRICE);
        coffee4 = new ConcreteCoffee(COFFEE3_NAME, COFFEE3_PRICE);

        vendor2 = new Vendor(VENDOR2_NAME, VENDOR2_LOCATION, VENDOR2_ID);
        coffee3 = new ConcreteCoffee(COFFEE2_NAME, COFFEE2_PRICE);

        vendorRepository.saveVendor(vendor);
        vendorRepository.saveVendor(vendor2);
        vendorRepository.saveCoffeeToVendor(vendor, coffee);
        vendorRepository.saveCoffeeToVendor(vendor, coffee4);
        vendorRepository.saveCoffeeToVendor(vendor2, coffee3);

        topping1 = new Topping(TOPPING_NAME1, TOPPING_PRICE1);
        vendorRepository.saveToppingToCoffee(coffee, topping1);
        topping2 = new Topping(TOPPING_NAME2, TOPPING_PRICE2);
        vendorRepository.saveToppingToCoffee(coffee, topping2);

        ToppingDecorator coffee1 = new ToppingDecorator(coffee, topping1);
        ToppingDecorator coffee2 = new ToppingDecorator(coffee1, topping2);
        coffeeBotUser.addItemToKeranjang(coffee2);
    }

    @ParameterizedTest
    @CsvSource({
            "'pesan', '" + VENDOR_ID + " " + COFFEE_NAME + "', 'ADDTOPPING'",
            "'beli', '', 'CONFIRM'",
    })
    void testHomeStateDoCommandChangeToCorrectState(String cmd, String param, String state) {
        coffeeBotUser.doCommand(cmd, param);

        CoffeeBotState expectedState = null;
        switch (state) {
            case "ADDTOPPING":
                expectedState = coffeeBotUser.getAddToppingCoffeeBotState();
                break;
            case "CONFIRM":
                expectedState = coffeeBotUser.getOrderConfirmationCoffeeBotState();
                break;
            default:
                break;
        }
        Assertions.assertEquals(expectedState, coffeeBotUser.getCurrentState());
    }

    @ParameterizedTest
    @CsvSource({
            "'', ''",
            "'vendor', '" + VENDOR_ID + " " + COFFEE_NAME + "'",
            "'menu', '" + VENDOR_ID + "'",
            "'keranjang', ''",
            "'help', ''",
            "'BLABLABLA', ''",
            "'pesan', 'BLABLA'", //wrong vendor_id and coffee_name
            "'pesan', '" + VENDOR_ID + " " + "BLABLA" + "'", //wrong coffee_name
    })
    void testHomeStateDoCommandDoNotChangeState(String cmd, String param) {
        coffeeBotUser.getCurrentState().doCommand(cmd, param);
        Assertions.assertEquals(coffeeBotUser.getHomeCoffeeBotState(),
                coffeeBotUser.getCurrentState());
    }

    @ParameterizedTest
    @CsvSource({
            "'', ''",
            "'pesan', '" + VENDOR_ID + " " + COFFEE_NAME + "'",
            "'beli', ''",
    })
    void testHomeStateDoCommandReturnCorrectCurrentStateResponse(String cmd, String param) {
        String res = coffeeBotUser.getCurrentState().doCommand(cmd, param);
        Assertions.assertEquals(coffeeBotUser.getCurrentStateMessage(), res);
    }

    @Test
    void testBeliDontChangeStateWhenKeranjangIsEmpty() {
        coffeeBotUser.clearKeranjang();
        coffeeBotUser.getCurrentState().doCommand("beli", "");
        Assertions.assertEquals(coffeeBotUser.getHomeCoffeeBotState(),
                coffeeBotUser.getCurrentState());
    }

    @ParameterizedTest
    @CsvSource({
            //wrong vendor_id and coffee_name
            "'pesan', 'BLABLA', 'Maaf, vendor'",
            "'pesan', 'sbjka', 'Maaf, vendor'",
            //wrong coffee_name
            "'pesan', '" + VENDOR_ID + " " + "BLABLABLABLABLA" + "', 'Maaf, minuman'",
            "'pesan', '" + VENDOR_ID + " " + "Black" + "', 'Maaf, minuman'",
            //wrong vendor_id
            "'menu', 'BLABLA', 'Maaf, vendor'",
            //list-topping
            "'list-topping', 'BLABLA', 'Maaf,'",
            "'list-topping', 'BLABLA HAHA', 'Maaf,'",
            "'list-topping', '1', 'List Topping menu'",
            //tambah-topping
            "'tambah-topping', 'BLABLA', 'Maaf,'",
            "'tambah-topping', 'BLABLA HAHA HIHI', 'Maaf,'",
            "'tambah-topping', '1 BLABLA', 'salah atau tidak ditemukan'",
            "'tambah-topping', '1 Boba', 'Anda telah menambahkan topping'",
            "'tambah-topping', '5 Boba', 'Maaf,'",
            "'help', '', 'Anda sedang berada di Halaman Depan CoffeeBot!'",
            "'BLABLABLABLAAAAA', '', 'yang diberikan tidak dikenal oleh CoffeeBot.'",
    })
    void testHomeStateDoCommandReturnCorrectCustomResponse(String cmd, String param, String exp) {
        String res = coffeeBotUser.getCurrentState().doCommand(cmd, param);
        Assertions.assertTrue(res.contains(exp));
    }

    @ParameterizedTest
    @CsvSource({
            "'hapus-pesanan', '1', 'telah dihapus dari keranjang'",
            "'hapus-pesanan', '5', 'Maaf, nomor urut'",
            "'hapus-pesanan', '-5', 'Maaf, nomor urut'",
            "'hapus-pesanan', 'THIS IS NOT A NUMBER', 'Maaf, nomor urut'",
    })
    void testHapusPesananWithParameterReturnCorrectResponse(String cmd, String param, String exp) {
        coffeeBotUser.addItemToKeranjang(coffee);

        String res = coffeeBotUser.getCurrentState().doCommand(cmd, param);
        Assertions.assertTrue(res.contains(exp));
    }

    @Test
    void testHapusPesananWithZeroAsParameterShouldReturnCorrectResponse() {
        coffeeBotUser.clearKeranjang();
        Assertions.assertEquals(0, coffeeBotUser.getSizeOfKeranjang());

        String cmd = "hapus-pesanan";
        String param = "0";
        String res = coffeeBotUser.getCurrentState().doCommand(cmd, param);
        Assertions.assertTrue(res.contains("Maaf, nomor urut"));

        Assertions.assertEquals(0, coffeeBotUser.getSizeOfKeranjang());
    }

    @Test
    void testHomeStateDoCommandVendorReturnCorrectResponse() {
        String cmd = "vendor";
        String param = "";
        String res = coffeeBotUser.getCurrentState().doCommand(cmd, param);
        Assertions.assertTrue(res.contains(vendor.getDescription()));
    }

    @Test
    void testHomeStateDoCommandMenuReturnCorrectResponse() {
        String cmd = "menu";
        String param = VENDOR_ID;
        String res = coffeeBotUser.getCurrentState().doCommand(cmd, param);
        Assertions.assertEquals(vendor.getMenu(), res);
    }

    @Test
    void testHomeStateDoCommandKeranjangReturnCorrectResponse() {
        String cmd = "keranjang";
        String param = "";
        String res = coffeeBotUser.getCurrentState().doCommand(cmd, param);
        Assertions.assertTrue(res.contains(coffeeBotUser.getAllItemInKeranjang()));
    }

    @Test
    void testHomeStateDoCommandPesanShouldAddToKeranjang() {
        String cmd = "pesan";
        String param = VENDOR_ID + " " + COFFEE_NAME;
        coffeeBotUser.getCurrentState().doCommand(cmd, param);
        Assertions.assertEquals(2, coffeeBotUser.getSizeOfKeranjang());
    }

    @ParameterizedTest
    @CsvSource({
            "'hapus-topping', 'BLABLA'",
            "'hapus-topping', 'BLABLA" + TOPPING_NAME1 + "'",
            "'hapus-topping', '-1 " + TOPPING_NAME1 + "'",
            "'hapus-topping', '5 " + TOPPING_NAME1 + "'",
            "'hapus-topping', '1 " + "TOPPING_ASAL" + "'",
    })
    void testHapusToppingWithWrongParameterShouldReturnCorrectResponse(String cmd, String param) {
        String res = coffeeBotUser.getCurrentState().doCommand(cmd, param);
        Assertions.assertTrue(res.contains("Maaf"));
    }

    @ParameterizedTest
    @CsvSource({
            "'hapus-topping', '1 " + TOPPING_NAME1 + "'",
            "'hapus-topping', '1 " + TOPPING_NAME2 + "'",
    })
    void testHapusToppingWithRightParameterShouldReturnCorrectResponse(String cmd, String param) {
        String res = coffeeBotUser.getCurrentState().doCommand(cmd, param);
        Assertions.assertTrue(res.contains("Topping"));
        Assertions.assertTrue(res.contains("telah dihapus dari pesananmu"));
    }

    @Test
    void testHomeStateGetMessage() {
        String res = this.coffeeBotUser.getCurrentState().getMessage();
        Assertions.assertTrue(res.contains("Selamat datang di CoffeeBot!"));
    }

    @Test
    void testHandlePesanCoffeeFromDifferentVendorWhenKeranjangEmpty() {
        coffeeBotUser.clearKeranjang();
        coffeeBotUser.getCurrentState().doCommand("pesan",
                VENDOR2_ID + " " + COFFEE2_NAME);

        Assertions.assertEquals(coffeeBotUser.getAddToppingCoffeeBotState(),
                coffeeBotUser.getCurrentState());
    }

    @Test
    void testHandlePesanCoffeeFromDifferentVendor() {
        String res = coffeeBotUser.getCurrentState().doCommand("pesan",
                VENDOR2_ID + " " + COFFEE2_NAME);

        Assertions.assertTrue(res.contains("Maaf, sudah ada pesanan dari vendor lain dengan id:"));
        Assertions.assertTrue(res.contains(VENDOR_ID));
        Assertions.assertTrue(res.contains(VENDOR2_ID));
    }

    @Test
    void testClearKeranjang() {
        Assertions.assertEquals(1, coffeeBotUser.getSizeOfKeranjang());
        String res = coffeeBotUser.getCurrentState().doCommand("hapus-keranjang", "");

        Assertions.assertEquals(0, coffeeBotUser.getSizeOfKeranjang());
        Assertions.assertTrue(res.contains("Semua pesanan di keranjang Anda telah dihapus."));
    }
}