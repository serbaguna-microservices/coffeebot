package csui.serbagunabot.coffeebot.chat.states;

import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


class OrderConfirmationCoffeeBotStateTest {

    private CoffeeBotUser coffeeBotUser;

    private static final String USER_ID = "azricot";

    @BeforeEach
    public void setUp() {
        coffeeBotUser = new CoffeeBotUser(USER_ID);
        coffeeBotUser.setCurrentState(this.coffeeBotUser.getOrderConfirmationCoffeeBotState());
    }

    @ParameterizedTest
    @CsvSource({
            "'konfirmasi', '', 'Terima kasih telah memesan di CoffeeBot!'",
            "'kembali', '', 'Selamat datang di CoffeeBot!'",
            "'help', '', 'Anda sedang berada di bagian konfirmasi pembelian'",
            "'BLABLABLABLAAAAA', '', 'yang diberikan tidak dikenal oleh CoffeeBot.'",
    })
    void testOrderConfirmStateDoCommandReturnCorrectResponse(String cmd, String param, String exp) {
        String res = this.coffeeBotUser.getCurrentState().doCommand(cmd, param);
        Assertions.assertTrue(res.contains(exp));
    }

    @Test
    void testOrderConfirmationStateDoCommandKonfirmasiChangeStateToMakingOrderState() {
        String command = "konfirmasi";
        String parameter = "";
        this.coffeeBotUser.getCurrentState().doCommand(command, parameter);
        Assertions.assertEquals(this.coffeeBotUser.getCurrentState(),
                this.coffeeBotUser.getMakingOrderCoffeeBotState());
    }

    @Test
    void testOrderConfirmationStateDoCommandKembaliChangeStateToHomeState() {
        String command = "kembali";
        String parameter = "";
        this.coffeeBotUser.getCurrentState().doCommand(command, parameter);
        Assertions.assertEquals(this.coffeeBotUser.getCurrentState(),
                this.coffeeBotUser.getHomeCoffeeBotState());
    }

    @Test
    void testOrderConfirmationStateDoCommandHelpDoesNotChangeState() {
        String command = "help";
        String parameter = "";
        this.coffeeBotUser.getCurrentState().doCommand(command, parameter);
        Assertions.assertEquals(this.coffeeBotUser.getCurrentState(),
                this.coffeeBotUser.getOrderConfirmationCoffeeBotState());
    }

    @Test
    void testOrderConfirmationStateGetMessage() {
        String res = this.coffeeBotUser.getCurrentState().getMessage();
        Assertions.assertTrue(res.contains("Apakah Anda sudah yakin untuk membeli item-item ini?"));
    }
}