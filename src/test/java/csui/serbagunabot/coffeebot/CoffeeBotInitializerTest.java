package csui.serbagunabot.coffeebot;

import csui.serbagunabot.coffeebot.repository.VendorRepository;
import csui.serbagunabot.coffeebot.repository.VendorRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;


class CoffeeBotInitializerTest {

    @InjectMocks
    private CoffeeBotInitializer coffeeBotInitializer;

    private VendorRepository vendorRepository;


    @BeforeEach
    public void setUp() {
        coffeeBotInitializer = new CoffeeBotInitializer();
        coffeeBotInitializer.init();
        vendorRepository = VendorRepositoryImpl.getUniqueVendorRepository();
    }

    @Test
    void testAllObjectsIsInitialized() {
        Assertions.assertEquals("Starbucks, Jakarta (ID: sbjkt)",
                vendorRepository.getVendorById("sbjkt").getDescription());
    }

}