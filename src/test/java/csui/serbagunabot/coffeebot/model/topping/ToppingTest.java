package csui.serbagunabot.coffeebot.model.topping;

import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class ToppingTest {

    private CoffeeBotUser coffeeBotUser;
    private Topping topping;

    private static final String USER_ID = "azricot";
    private static final String TOPPING_NAME = "Boba";
    private static final int TOPPING_PRICE = 5000;

    @BeforeEach
    public void setUp() {
        coffeeBotUser = new CoffeeBotUser(USER_ID);
        topping = new Topping(TOPPING_NAME, TOPPING_PRICE);
    }

    @Test
    void testToppingGetDescription() {
        Assertions.assertEquals(TOPPING_NAME, topping.getDescription());
    }

    @Test
    void testToppingCost() {
        Assertions.assertEquals(TOPPING_PRICE, topping.cost());
    }
}