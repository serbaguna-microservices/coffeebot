package csui.serbagunabot.coffeebot.model.topping;

import csui.serbagunabot.coffeebot.model.coffee.Coffee;
import csui.serbagunabot.coffeebot.model.coffee.ConcreteCoffee;
import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import csui.serbagunabot.coffeebot.model.vendor.Vendor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class ToppingDecoratorTest {

    private CoffeeBotUser coffeeBotUser;
    private Topping topping1;
    private Topping topping2;
    private Coffee coffee;
    private Coffee coffeeWithOneTopping;

    private static final String USER_ID = "azricot";
    private static final String TOPPING1_NAME = "Boba";
    private static final int TOPPING1_PRICE = 5000;
    private static final String TOPPING2_NAME = "Susu";
    private static final int TOPPING2_PRICE = 6000;
    private static final String COFFEE_NAME = "Caffe Latte";
    private static final int COFFEE_PRICE = 50000;


    @BeforeEach
    public void setUp() {
        coffeeBotUser = new CoffeeBotUser(USER_ID);
        coffee = new ConcreteCoffee(COFFEE_NAME, COFFEE_PRICE);
        topping1 = new Topping(TOPPING1_NAME, TOPPING1_PRICE);
        topping2 = new Topping(TOPPING2_NAME, TOPPING2_PRICE);

        coffeeWithOneTopping = new ToppingDecorator(coffee, topping1);
        coffeeWithOneTopping.addToppingToAllowedToppings(topping2);
    }

    @Test
    void testDecoratorGetDescriptionShouldContainCoffeeAndToppingName() {
        String coffeeDescription = coffeeWithOneTopping.getDescription();
        Assertions.assertTrue(coffeeDescription.contains(COFFEE_NAME));
        Assertions.assertTrue(coffeeDescription.contains(TOPPING1_NAME));
    }

    @Test
    void testDecoratorGetDescriptionForMultipleTopping() {
        Coffee coffeeWithTwoTopping = new ToppingDecorator(coffeeWithOneTopping, topping2);

        String coffeeDescription = coffeeWithTwoTopping.getDescription();
        Assertions.assertTrue(coffeeDescription.contains(COFFEE_NAME));
        Assertions.assertTrue(coffeeDescription.contains(TOPPING1_NAME));
        Assertions.assertTrue(coffeeDescription.contains(TOPPING2_NAME));
    }

    @Test
    void testDecoratorCostShouldBeCoffeeCostPlusToppingCost() {
        Assertions.assertEquals(
                COFFEE_PRICE + TOPPING1_PRICE,
                coffeeWithOneTopping.cost());
    }

    @Test
    void testDecoratorCostForMultipleTopping() {
        Coffee coffeeWithTwoTopping = new ToppingDecorator(coffeeWithOneTopping, topping2);

        Assertions.assertEquals(
                COFFEE_PRICE + TOPPING1_PRICE + TOPPING2_PRICE,
                coffeeWithTwoTopping.cost());
    }

    @Test
    void testSetIdVendor() {
        Assertions.assertNull(coffee.getIdVendor());

        coffeeWithOneTopping.setIdVendor("ID");
        Assertions.assertEquals("ID", coffeeWithOneTopping.getIdVendor());
    }

    @Test
    void testGetAllowedToppingsAsString() {
        String res = coffeeWithOneTopping.getAllowedToppingsAsString();

        Assertions.assertTrue(res.contains(TOPPING2_NAME));
    }

}