package csui.serbagunabot.coffeebot.model.line;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LineResponseTest {

    private LineResponse lineResponse;
    private static final String SAMPLE_RESPONSE = "This is a message";

    @BeforeEach
    void setUp() {
        lineResponse = new LineResponse(SAMPLE_RESPONSE);
    }

    @Test
    void testResponseGetterShouldBeCorrect() {
        Assertions.assertEquals(SAMPLE_RESPONSE, lineResponse.getResponse());
    }

}