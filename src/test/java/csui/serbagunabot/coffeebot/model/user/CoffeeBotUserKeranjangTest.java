package csui.serbagunabot.coffeebot.model.user;

import csui.serbagunabot.coffeebot.model.coffee.Coffee;
import csui.serbagunabot.coffeebot.model.coffee.ConcreteCoffee;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CoffeeBotUserKeranjangTest {

    private CoffeeBotUserKeranjang coffeeBotUserKeranjang;

    private Coffee coffee1;
    private Coffee coffee2;

    private static final String COFFEE1_NAME = "Caffe Latte";
    private static final int COFFEE1_PRICE = 50000;
    private static final String COFFEE2_NAME = "Dark Mocha";
    private static final int COFFEE2_PRICE = 60000;

    @BeforeEach
    public void setUp() {
        coffeeBotUserKeranjang = new CoffeeBotUserKeranjang();

        coffee1 = new ConcreteCoffee(COFFEE1_NAME, COFFEE1_PRICE);
        coffee2 = new ConcreteCoffee(COFFEE2_NAME, COFFEE2_PRICE);
    }

    @Test
    void testGetSizeInitialShouldBeZero() {
        Assertions.assertEquals(0, coffeeBotUserKeranjang.getSize());
    }

    @Test
    void testAddItemToKeranjang() {
        Assertions.assertEquals(0, coffeeBotUserKeranjang.getSize());
        coffeeBotUserKeranjang.addItemToKeranjang(coffee1);
        Assertions.assertEquals(1, coffeeBotUserKeranjang.getSize());
    }

    @Test
    void testGetAllItemInKeranjangWhenKeranjangIsEmpty() {
        String res = coffeeBotUserKeranjang.getAllItemInKeranjang();
        Assertions.assertTrue(res.contains("Keranjang Anda (0 item):"));
        Assertions.assertTrue(res.contains("Belum ada minuman dalam keranjang Anda."));
    }

    @Test
    void testGetAllItemInKeranjangWhenKeranjangIsNotEmpty() {
        coffeeBotUserKeranjang.addItemToKeranjang(coffee1);
        String res = coffeeBotUserKeranjang.getAllItemInKeranjang();

        Assertions.assertTrue(res.contains("Keranjang Anda ("));
        Assertions.assertTrue(res.contains(" item):"));
        Assertions.assertTrue(res.contains(COFFEE1_NAME));
    }

    @Test
    void testGetTotalPrice() {
        double totalPrice = coffeeBotUserKeranjang.getTotalPriceInKeranjang();
        Assertions.assertEquals(0, totalPrice);

        coffeeBotUserKeranjang.addItemToKeranjang(coffee1);
        coffeeBotUserKeranjang.addItemToKeranjang(coffee2);

        totalPrice = coffeeBotUserKeranjang.getTotalPriceInKeranjang();
        Assertions.assertEquals(COFFEE1_PRICE + COFFEE2_PRICE, totalPrice);
    }

    @Test
    void testRemoveItemFromKeranjang() {
        Assertions.assertEquals(0, coffeeBotUserKeranjang.getSize());
        coffeeBotUserKeranjang.addItemToKeranjang(coffee1);
        coffeeBotUserKeranjang.addItemToKeranjang(coffee2);
        Assertions.assertEquals(2, coffeeBotUserKeranjang.getSize());

        coffeeBotUserKeranjang.removeItemFromKeranjang(coffee1);
        Assertions.assertEquals(1, coffeeBotUserKeranjang.getSize());

        String res = coffeeBotUserKeranjang.getAllItemInKeranjang();
        Assertions.assertFalse(res.contains(COFFEE1_NAME));
        Assertions.assertTrue(res.contains(COFFEE2_NAME));
    }

    @Test
    void testRemoveItemFromKeranjangByIdx() {
        Assertions.assertEquals(0, coffeeBotUserKeranjang.getSize());
        coffeeBotUserKeranjang.addItemToKeranjang(coffee1);
        coffeeBotUserKeranjang.addItemToKeranjang(coffee2);
        Assertions.assertEquals(2, coffeeBotUserKeranjang.getSize());

        coffeeBotUserKeranjang.removeItemFromKeranjangByIdx(0);
        Assertions.assertEquals(1, coffeeBotUserKeranjang.getSize());

        String res = coffeeBotUserKeranjang.getAllItemInKeranjang();
        Assertions.assertFalse(res.contains(COFFEE1_NAME));
        Assertions.assertTrue(res.contains(COFFEE2_NAME));
    }

    @Test
    void testGetLastAddedItem() {
        coffeeBotUserKeranjang.addItemToKeranjang(coffee1);
        coffeeBotUserKeranjang.addItemToKeranjang(coffee2);

        Coffee res = coffeeBotUserKeranjang.getLastAddedItemToKeranjang();

        Assertions.assertEquals(coffee2, res);
        Assertions.assertEquals(2, coffeeBotUserKeranjang.getSize());
    }

    @Test
    void testPopLastAddedItem() {
        coffeeBotUserKeranjang.addItemToKeranjang(coffee1);
        coffeeBotUserKeranjang.addItemToKeranjang(coffee2);

        Coffee res = coffeeBotUserKeranjang.popLastAddedItemToKeranjang();

        Assertions.assertEquals(coffee2, res);
        Assertions.assertEquals(1, coffeeBotUserKeranjang.getSize());
    }

    @Test
    void testClearKeranjang() {
        Assertions.assertEquals(0, coffeeBotUserKeranjang.getSize());
        coffeeBotUserKeranjang.addItemToKeranjang(coffee1);
        coffeeBotUserKeranjang.addItemToKeranjang(coffee2);
        Assertions.assertEquals(2, coffeeBotUserKeranjang.getSize());

        coffeeBotUserKeranjang.clearKeranjang();
        Assertions.assertEquals(0, coffeeBotUserKeranjang.getSize());
    }

}