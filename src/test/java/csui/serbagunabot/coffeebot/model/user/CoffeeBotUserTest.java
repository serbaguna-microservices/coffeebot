package csui.serbagunabot.coffeebot.model.user;

import static org.mockito.Mockito.*;

import csui.serbagunabot.coffeebot.chat.states.AddToppingCoffeeBotState;
import csui.serbagunabot.coffeebot.chat.states.HomeCoffeeBotState;
import csui.serbagunabot.coffeebot.chat.states.OrderConfirmationCoffeeBotState;
import csui.serbagunabot.coffeebot.handler.MessagePushHandler;
import csui.serbagunabot.coffeebot.model.coffee.Coffee;
import csui.serbagunabot.coffeebot.model.coffee.ConcreteCoffee;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class CoffeeBotUserTest {

    @Mock
    MessagePushHandler messagePushHandler;

    @Mock
    private HomeCoffeeBotState homeCoffeeBotState;

    @Mock
    private AddToppingCoffeeBotState addToppingCoffeeBotState;

    private CoffeeBotUser coffeeBotUser;

    private static final String USER_ID = "azricot";
    private static final String PUSH_MSG = "This is a push message.";

    @BeforeEach
    public void setUp() {
        coffeeBotUser = new CoffeeBotUser(USER_ID);
    }

    @Test
    void testInitialStateIsHomeCoffeeBotState() {
        Assertions.assertTrue(coffeeBotUser.getCurrentState() instanceof HomeCoffeeBotState);
    }

    @Test
    void testGetUserId() {
        Assertions.assertEquals(USER_ID, coffeeBotUser.getUserId());
    }

    @Test
    void testSetCurrentStates() {
        Assertions.assertTrue(coffeeBotUser.getCurrentState() instanceof HomeCoffeeBotState);

        coffeeBotUser.setCurrentState(coffeeBotUser.getAddToppingCoffeeBotState());
        Assertions.assertTrue(coffeeBotUser.getCurrentState() instanceof AddToppingCoffeeBotState);

        coffeeBotUser.setCurrentState(coffeeBotUser.getOrderConfirmationCoffeeBotState());
        Assertions.assertTrue(
                coffeeBotUser.getCurrentState() instanceof OrderConfirmationCoffeeBotState
        );
    }

    @Test
    void testDoCommandCallsCurrentStatesDoCommand() {
        String command = "Command";
        String message = "Message";

        coffeeBotUser.setCurrentState(homeCoffeeBotState);
        coffeeBotUser.doCommand(command, message);
        verify(homeCoffeeBotState, times(1)).doCommand(command, message);

        coffeeBotUser.setCurrentState(addToppingCoffeeBotState);
        coffeeBotUser.doCommand(command, message);
        verify(addToppingCoffeeBotState, times(1)).doCommand(command, message);
    }

    @Test
    void testGetCurrentStateMessage() {
        Assertions.assertTrue(coffeeBotUser.getCurrentState() instanceof HomeCoffeeBotState);

        Assertions.assertEquals(coffeeBotUser.getCurrentStateMessage(),
                coffeeBotUser.getHomeCoffeeBotState().getMessage());
    }

    @Test
    void testAddItemToKeranjang() {
        Assertions.assertEquals(0, coffeeBotUser.getSizeOfKeranjang());

        coffeeBotUser.addItemToKeranjang(new ConcreteCoffee("Latte", 50000));
        coffeeBotUser.addItemToKeranjang(new ConcreteCoffee("Kopi-Hitam", 20000));

        Assertions.assertEquals(2, coffeeBotUser.getSizeOfKeranjang());
    }

    @Test
    void testRemoveItemFromKeranjang() {
        Coffee coffee1 = new ConcreteCoffee("Latte", 50000);
        Coffee coffee2 = new ConcreteCoffee("Kopi-Hitam", 20000);

        coffeeBotUser.getKeranjang().addItemToKeranjang(coffee1);
        coffeeBotUser.addItemToKeranjang(coffee2);

        Assertions.assertEquals(2, coffeeBotUser.getSizeOfKeranjang());

        coffeeBotUser.removeItemFromKeranjang(coffee2);

        Assertions.assertEquals(1, coffeeBotUser.getSizeOfKeranjang());
    }

    @Test
    void testGetAllItemInKeranjang() {
        Assertions.assertFalse(coffeeBotUser.getAllItemInKeranjang().contains("Latte")
                && coffeeBotUser.getAllItemInKeranjang().contains("Kopi-Hitam"));

        coffeeBotUser.addItemToKeranjang(new ConcreteCoffee("Latte", 50000));
        coffeeBotUser.addItemToKeranjang(new ConcreteCoffee("Kopi-Hitam", 20000));

        Assertions.assertTrue(coffeeBotUser.getAllItemInKeranjang().contains("Latte")
                && coffeeBotUser.getAllItemInKeranjang().contains("Kopi-Hitam"));
    }

    @Test
    void testMessagePushHandlerGetter() {
        coffeeBotUser.setMessagePushHandler(messagePushHandler);
        Assertions.assertEquals(
                messagePushHandler, coffeeBotUser.getMessagePushHandler()
        );
    }

    @Test
    void testPushMessage() {
        coffeeBotUser.setMessagePushHandler(messagePushHandler);

        coffeeBotUser.pushMessage(PUSH_MSG);
        verify(messagePushHandler, times(1)).handlePushMessage(PUSH_MSG, USER_ID);
    }
}