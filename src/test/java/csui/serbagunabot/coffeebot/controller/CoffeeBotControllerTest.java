package csui.serbagunabot.coffeebot.controller;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.serbagunabot.coffeebot.model.line.LineRequest;
import csui.serbagunabot.coffeebot.service.CoffeeBotHandlerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


@SpringBootTest
@AutoConfigureMockMvc
class CoffeeBotControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    CoffeeBotHandlerService coffeeBotHandlerService;

    LineRequest lineRequest;

    private static final String SAMPLE_RESPONSE = "This is a response";
    private static final String SAMPLE_USERID = "azricot";
    private static final String SAMPLE_MSG = "This is a message";

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }


    @BeforeEach
    void setUp() {
        lineRequest = new LineRequest();
        lineRequest.setMessage(SAMPLE_MSG);
        lineRequest.setUserId(SAMPLE_USERID);
    }

    @Test
    void testReplyEntryPointTransportasiShouldReturnJson() throws Exception {
        when(coffeeBotHandlerService
                .handleMessage(anyString(), anyString()))
                .thenReturn(SAMPLE_RESPONSE);

        mvc.perform(post("/")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(lineRequest)))
                .andExpect(jsonPath("$.response")
                        .value(SAMPLE_RESPONSE));
    }
}