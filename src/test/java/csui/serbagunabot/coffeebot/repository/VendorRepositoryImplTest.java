package csui.serbagunabot.coffeebot.repository;

import csui.serbagunabot.coffeebot.model.coffee.Coffee;
import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import csui.serbagunabot.coffeebot.model.vendor.Vendor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class VendorRepositoryImplTest {

    @InjectMocks
    private VendorRepositoryImpl vendorRepository;

    @Mock
    private Vendor vendor;

    private CoffeeBotUser coffeeBotUser;

    private static final String USER_ID = "azricot";

    @BeforeEach
    public void setUp() {
        coffeeBotUser = new CoffeeBotUser(USER_ID);
    }

    @Test
    void testgetCoffeeByVendorAndNameGivenWrongParamShoulReturnNull() {
        Coffee coffee = vendorRepository.getCoffeeByVendorAndName(vendor, "BLABLA");
        Assertions.assertNull(coffee);
    }

    @Test
    void testgetVendorByIdGivenWrongParamShoulReturnNull() {
        Vendor vendor1 = vendorRepository.getVendorById("BLABLA");
        Assertions.assertNull(vendor1);
    }

    @Test
    void testgetVendorDescByIdGivenWrongParamShoulReturnNull() {
        String desc = vendorRepository.getVendorDescById("BLABLA");
        Assertions.assertNull(desc);
    }
}