package csui.serbagunabot.coffeebot.repository;

import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;


@ExtendWith(MockitoExtension.class)
class CoffeeBotUserRepositoryImplTest {

    @Mock
    AutowireCapableBeanFactory autowireCapableBeanFactory;

    @InjectMocks
    private CoffeeBotUserRepositoryImpl coffeeBotUserRepository;

    private CoffeeBotUser coffeeBotUser;

    @Test
    void testCreateUserFromId() {
        coffeeBotUser = coffeeBotUserRepository.createUserFromId("SECRET_ID");

        Assertions.assertNotNull(coffeeBotUser);
    }

    @Test
    void testFindByUserIdShouldReturnNullIfUserNotFound() {
        coffeeBotUser = coffeeBotUserRepository.findByUserId("SECRET_ID");

        Assertions.assertNull(coffeeBotUser);
    }

    @Test
    void testFindByUserIdShouldReturnUserIfUserIsFound() {
        coffeeBotUserRepository.createUserFromId("SECRET_ID");
        coffeeBotUser = coffeeBotUserRepository.findByUserId("SECRET_ID");

        Assertions.assertNotNull(coffeeBotUser);
    }
}