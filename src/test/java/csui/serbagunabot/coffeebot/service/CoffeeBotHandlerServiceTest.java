package csui.serbagunabot.coffeebot.service;

import static org.mockito.Mockito.*;

import csui.serbagunabot.coffeebot.model.user.CoffeeBotUser;
import csui.serbagunabot.coffeebot.repository.CoffeeBotUserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class CoffeeBotHandlerServiceTest {

    @InjectMocks
    private CoffeeBotHandlerService coffeeBotHandlerService;

    @Mock
    private CoffeeBotUserRepository coffeeBotUserRepository;

    private CoffeeBotUser coffeeBotUser;
    private static final String MESSAGE_1 = "!coffee";
    private static final String MESSAGE_2 = "!coffee vendor menu";
    private static final String USER_ID = "azr";

    @BeforeEach
    public void setUp() {
        coffeeBotUser = new CoffeeBotUser(USER_ID);
    }

    @Test
    void testMessageHandlerShouldReturnUserIfUserIsFound() {
        when(coffeeBotUserRepository.findByUserId(USER_ID)).thenReturn(coffeeBotUser);

        String res = coffeeBotHandlerService.handleMessage(MESSAGE_1, USER_ID);
        verify(coffeeBotUserRepository, times(0)).createUserFromId(USER_ID);
        verify(coffeeBotUserRepository, times(1)).findByUserId(USER_ID);
        Assertions.assertNotNull(res);
    }

    @Test
    void testMessageHandlerShouldCreateUserIfUserIsNotFound() {
        when(coffeeBotUserRepository.createUserFromId(USER_ID)).thenReturn(coffeeBotUser);

        String res = coffeeBotHandlerService.handleMessage(MESSAGE_2, USER_ID);
        verify(coffeeBotUserRepository, times(1)).createUserFromId(USER_ID);
        verify(coffeeBotUserRepository, times(1)).findByUserId(USER_ID);
        Assertions.assertNotNull(res);
    }
}