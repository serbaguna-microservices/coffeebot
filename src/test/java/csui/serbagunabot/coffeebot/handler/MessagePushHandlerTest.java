package csui.serbagunabot.coffeebot.handler;

import static org.mockito.Mockito.*;

import csui.serbagunabot.coffeebot.model.line.LineRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class MessagePushHandlerTest {

    @Mock
    CoffeeBotPushMessageFeign coffeeBotPushMessageFeign;

    @InjectMocks
    MessagePushHandler messagePushHandler;

    private static final String SAMPLE_USERID = "azricot";
    private static final String SAMPLE_MSG = "This is a message";

    @Test
    void testHandlePushMessageShouldCallCoffeeBotPushMessageFeign() {
        messagePushHandler.handlePushMessage(SAMPLE_MSG, SAMPLE_USERID);
        verify(coffeeBotPushMessageFeign, times(1))
                .handleCoffeebotMessage(any(LineRequest.class));

    }


}