## CoffeeBot Microservice (TK Adpro A7)
[![pipeline status](https://gitlab.com/serbaguna-microservices/coffeebot/badges/master/pipeline.svg)](https://gitlab.com/serbaguna-microservices/coffeebot/-/commits/master)
[![coverage report](https://gitlab.com/serbaguna-microservices/coffeebot/badges/master/coverage.svg)](https://gitlab.com/serbaguna-microservices/coffeebot/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=serbaguna-microservices_coffeebot&metric=alert_status)](https://sonarcloud.io/dashboard?id=serbaguna-microservices_coffeebot)

Dibuat oleh Rico Tadjudin - 1906398364 sebagai bagian dari [Tugas Kelompok Adpro Kelompok A7](https://gitlab.com/serbaguna-microservices)

## SonarCloud
Proyek ini juga di-scan untuk Code Smells, Bugs, dan Vulnerabilities dengan SonarCloud yang bisa dilihat [link berikut](https://sonarcloud.io/dashboard?id=serbaguna-microservices_coffeebot)

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=serbaguna-microservices_coffeebot)](https://sonarcloud.io/dashboard?id=serbaguna-microservices_coffeebot)

## Links dan Line ID
Microservice ini di deploy di https://serbaguna-coffeebot.herokuapp.com/

Line ID OA: `@109tpdsd`
